@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->



<section>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-title-1">
                <h3 class="c-center c-font-uppercase c-font-bold">Product Models</h3>
                <div class="c-line-center"></div>
            </div>
            <!-- BEGIN: CONTENT/SHOPS/SHOP-2-7 -->
            <div class="c-bs-grid-small-space">

                <div class="row">
                    @foreach($productsModel as $productModel)

                    <div class="col-md-3 col-sm-6 c-margin-b-20">
                        <div class="c-content-product-2 c-bg-white c-border">
                            <div class="c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="{{ route('product_list',$productModel->id) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                    </div>
                                </div>
                                <div class="c-bg-img-center-contain c-overlay-object" data-height="height" style="height: 230px; background-image: url( {{ asset($productThumb)}});"></div>
                            </div>
                            <div class="c-info">
                                <p class="c-title c-font-18 c-font-slim">{{$productModel->title}}</p>
                                <p class="c-price c-font-18 c-font-slim">$500 +
                                </p>
                            </div>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group c-border-top" role="group">
                                    <a href="{{ route('product_list',$productModel->id) }}" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Buy</a>
                                </div>

                            </div>
                        </div>
                    </div>

                    @endforeach

                </div><!-- END: CONTENT/SHOPS/SHOP-2-7 -->

            </div>
        </div>
    </div>
</section>

<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->


@endsection
