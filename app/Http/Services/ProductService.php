<?php

namespace App\Http\Services;


use App\Product;


class ProductService{

    public function getProduct($product_id){
        $product = Product::find($product_id);
        return $product;
    }


    /*
    *
    */
    public function formValidationConstraints($request){
        $validator_data = [
            'price'                 => 'required',
            // 'email'                 => 'required',
            // 'date-expire'           => 'required|before:tomorrow|date_format:"d/m/Y"',
            // 'title'                 => 'required|max:255',
            // 'description'           => 'required',
            // 'condition'             => 'required',
            // 'nid'            => 'nullable',
            // 'nid_file'       => 'nullable|mimes:pdf,jpeg,png|max:2048',
        ];


        return $validator_data;
    }



}
