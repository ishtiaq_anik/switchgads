<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\MobileColor;
use App\Http\Controllers\Controller;
use App\Http\Datatables\MobileColorDatatable;
use Illuminate\Http\Request;

class MobileColorController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('mobile_color_index'),
            'dataload_url' => route('mobile_color_dataload'),
            'title' => "Mobile Color",
            'titles' => "Mobile Colors",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
        //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('mobile')->withSubnav('mobile-color');
    }

    public function datatable(Request $request) {
        $ajax_table = new MobileColorDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $mobilecolor = new MobileColor;
        $title = 'Mobile Color';
        $params = [
            'mobilecolor' => $mobilecolor,
            'route'    => route('mobile_color_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.mobile.mobile-color-create', $params)->withNav('mobile')->withSubnav('mobile-color');
    }

    /**
    * Store a newly created resource in color.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
         // var_dump($request->all());
         // die();

        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:255|unique:mobile_colors,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        if ($request->_ajax_id) {

            $mobilecolor = MobileColor::find($request->_ajax_id);
        } else {
            $mobilecolor = new MobileColor;
        }

        $mobilecolor->title = $request->input('title');
        $mobilecolor->slug = $request->input('slug');
        if($mobilecolor->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('mobile_color_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\MobileColor  $mobileColor
    * @return \Illuminate\Http\Response
    */
    public function show(MobileColor $mobileColor)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\MobileColor  $mobileColor
    * @return \Illuminate\Http\Response
    */
    public function edit($mobilecolor_id)
    {
        $mobilecolor = MobileColor::find($mobilecolor_id);
        if($mobilecolor){
            $title = 'Mobile Color';
            $params = [
                'mobilecolor' => $mobilecolor,
                'route'    => route('mobile_color_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.mobile.mobile-color-create', $params);
        }
        return redirect()->route('mobile_color_index')->withNav('mobiles');
    }


    public function subcolors(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $subcolors = MobileSubcolor::where('mobilecolor_id', $id)->get();
        if($subcolors) {
            $this->success();
            $this->message = "records found!";
            $this->data = $subcolors;
        }
        return $this->output();
    }

    /**
    * Update the specified resource in color.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\MobileColor  $mobileColor
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, MobileColor $mobileColor)
    {
        //
    }

    /**
    * Remove the specified resource from color.
    *
    * @param  \App\MobileColor  $mobileColor
    * @return \Illuminate\Http\Response
    */
    public function destroy(MobileColor $mobileColor)
    {
        //
    }
}
