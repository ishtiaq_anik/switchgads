<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Request;
use \App\User;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $adminRole = \App\Role::where('name','=','admin')->first();
        $sellerRole = \App\Role::where('name', '=', 'seller')->first();
        $buyerRole = \App\Role::where('name', '=', 'buyer')->first();

        $admin = User::create([
            'name' => "Arif",
            'email' => 'arif.saiket@gmail.com',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $admin->attachRole($adminRole);

        $admin2 = User::create([
            'name' => "Switchgads",
            'email' => 'switchgads@gmail.com',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $admin2->attachRole($adminRole);

        $testSeller = User::create([
            'name' => "Seller",
            'email' => 'apu.eee+1@gmail.com',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $testSeller->attachRole($sellerRole);

    }

}
