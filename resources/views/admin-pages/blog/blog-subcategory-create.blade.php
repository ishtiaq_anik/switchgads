@extends('admin.master')

@section('page_style_plugin')

<link href="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1> Subcategory {{ ($subcategory->id != "") ? __('edit'): __('create') }}
                    <small></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('subcategory_index') }}">Subcategories</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">{{ ($subcategory->id != "") ? __('edit') : __('create') }}</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->

        <div class="page-content-col">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <h3 class="c-center c-font-uppercase c-font-bold c-justify"><i class="icon-map"></i> {{$title}} </h3>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-multiple ajaxForm" id="ajaxForm" enctype='multipart/form-data'>
                                {{ csrf_field() }}
                                {{ ajax_action($route) }}
                                {{ ajax_method($method) }}
                                <?php if($subcategory->id){ ?>
                                    {{ ajax_id($subcategory->id) }}
                                <?php } ?>

                                <div class="row">
                                    <div class="col-lg-12">

                                        <!-- Category -->
                                        <div class="form-group row has-error-category">
                                            <label class="text-right col-md-4 control-label">Category<span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <select class="form-control " name="category" id="category">
                                                    <option value="0">Select</option>
                                                    @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" <?php if($subcategory->category_id == $category->id){echo "selected";}?> >
                                                        {{ $category->title}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Title -->
                                        <div class="form-group row has-error-title">
                                            <label class="text-right col-md-4 control-label">Title<span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control form-control-sm" placeholder="title"
                                                name="title" value="<?php if($subcategory->title){echo $subcategory->title;}else{echo old('title');} ?>">
                                            </div>
                                        </div>

                                        <!-- Code Bangla -->
                                        <div class="form-group row has-error-slug">
                                            <label class="text-right col-md-4 control-label">Slug <span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control form-control-sm" placeholder="slug"
                                                name="slug" value="<?php if($subcategory->slug){echo $subcategory->slug;}else{echo old('slug');} ?>">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group c-margin-t-40">
                                            <div class="col-sm-offset-5 col-md-7">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END PAGE BASE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
</div>
@endsection

@section('page_script_plugin')
<script src="{{asset('/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/custom.js')}}" type="text/javascript"></script>

@endsection
