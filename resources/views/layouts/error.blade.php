<!DOCTYPE html>

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"  >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    @include ('layouts.meta-content')
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/plugins/socicon/socicon.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-social/bootstrap-social.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/nprogress/nprogress.css') }}" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->

    @yield('page_style_one')

    <!-- BEGIN: BASE PLUGINS  -->
    <link href="{{ asset('assets/plugins/cubeportfolio/css/cubeportfolio.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/owl-carousel/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/slider-for-bootstrap/css/slider.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END: BASE PLUGINS -->


    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('assets/default/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/default/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/default/css/themes/default.css')}}" rel="stylesheet" id="style_theme" type="text/css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="c-layout-header-fixed c-layout-header-mobile-fixed">

    <script src="{{ asset('assets/plugins/nprogress/nprogress.js') }}" type="text/javascript" ></script>
    <script>NProgress.start();</script>

    @include ('layouts.header-master')

    {{--@include ('layouts.login')--}}

    <!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <div class="c-layout-page">
{{--@include ('layouts.search')--}}
        @yield ('content')

    </div>

    @include ('layouts.footer-master')


    <!-- BEGIN: CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
    <![endif]-->
    <script src="{{ asset('assets/plugins/jquery.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('assets/plugins/jquery-migrate.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('assets/plugins/jquery.easing.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('assets/plugins/reveal-animate/wow.js') }}" type="text/javascript" ></script>
    <script src="{{asset('assets/default/js/scripts/reveal-animate/reveal-animate.js')}}" type="text/javascript" ></script>
    <script src="{{asset('assets/plugins/jquery.blockUI.js')}}" type="text/javascript" ></script>


    <!-- END: CORE PLUGINS -->
    @yield('page_script_one')
    <!-- BEGIN: LAYOUT PLUGINS -->
    <script src="{{ asset('assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/fancybox/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/smooth-scroll/jquery.smooth-scroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/js-cookie/js.cookie.js') }}" type="text/javascript"></script>
    <!-- END: LAYOUT PLUGINS -->
    @yield('page_script_two')
    <!-- BEGIN: THEME SCRIPTS -->
    <script src="{{asset('assets/base/js/components.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/base/js/components-shop.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/base/js/app.js')}}" type="text/javascript"></script>
    <script>
    $(document).ready(function() {
        App.init(); // init core
    });
    </script>
    <!-- END: THEME SCRIPTS -->
    @yield('page_script_three')
    @yield('page_scripts')
    <!-- END: LAYOUT/BASE/BOTTOM -->

</body>
</html>
