<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\MobileStorage;
use App\Http\Controllers\Controller;
use App\Http\Datatables\MobileStorageDatatable;
use Illuminate\Http\Request;

class MobileStorageController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('mobile_storage_index'),
            'dataload_url' => route('mobile_storage_dataload'),
            'title' => "Mobile Storage",
            'titles' => "Mobile Storages",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
        //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('mobile')->withSubnav('mobile-storage');
    }

    public function datatable(Request $request) {
        $ajax_table = new MobileStorageDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $mobilestorage = new MobileStorage;
        $title = 'Mobile Storage';
        $params = [
            'mobilestorage' => $mobilestorage,
            'route'    => route('mobile_storage_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.mobile.mobile-storage-create', $params)->withNav('mobile')->withSubnav('mobile-storage');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
         // var_dump($request->all());
         // die();

        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:255|unique:mobile_storages,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        if ($request->_ajax_id) {

            $mobilestorage = MobileStorage::find($request->_ajax_id);
        } else {
            $mobilestorage = new MobileStorage;
        }

        $mobilestorage->title = $request->input('title');
        $mobilestorage->slug = $request->input('slug');
        if($mobilestorage->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('mobile_storage_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\MobileStorage  $mobileStorage
    * @return \Illuminate\Http\Response
    */
    public function show(MobileStorage $mobileStorage)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\MobileStorage  $mobileStorage
    * @return \Illuminate\Http\Response
    */
    public function edit($mobilestorage_id)
    {
        $mobilestorage = MobileStorage::find($mobilestorage_id);
        if($mobilestorage){
            $title = 'Mobile Storage';
            $params = [
                'mobilestorage' => $mobilestorage,
                'route'    => route('mobile_storage_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.mobile.mobile-storage-create', $params);
        }
        return redirect()->route('mobile_storage_index')->withNav('mobiles');
    }


    public function substorages(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $substorages = MobileSubstorage::where('mobilestorage_id', $id)->get();
        if($substorages) {
            $this->success();
            $this->message = "records found!";
            $this->data = $substorages;
        }
        return $this->output();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\MobileStorage  $mobileStorage
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, MobileStorage $mobileStorage)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\MobileStorage  $mobileStorage
    * @return \Illuminate\Http\Response
    */
    public function destroy(MobileStorage $mobileStorage)
    {
        //
    }
}
