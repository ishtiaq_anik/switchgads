<?php

namespace App\Http\Controllers;

use Validator;
use Auth;
use App\User;
use App\Product;
use App\MobileColor;
use App\MobileStorage;
use App\ProductPicture;
use App\MobileAccessory;
use App\MobileCondition;
use Illuminate\Http\Request;
use App\Models\Product\ProductModel;
use App\Models\Product\ProductCategory;
use App\Http\Services\EmailService;
use App\Http\Services\ProductService;

class ProductController extends Controller
{

    public function home() {

        //    $productTypes = ProductCategory::all();
        //    dd($this->productTypes->count());

        $params = [
            'productTypes' => $this->productTypes,
        ];

        return view('home',$params)->withNav('home');
    }
    public function productCategoriesShow($id)
    {

        //    $productsModel = ProductModel::all();
        $productsModel = ProductModel::where('product_category_id',$id)->get();
        //    dd($this->productTypes['title']);
        $nav = $id;
        $params = [
            'productsModel' => $productsModel,
            'productTypes' => $this->productTypes,
            'productThumb' => $this->getProductCategoryThumb($id),
        ];
        return view('product-categories', $params)->withNav($nav);
    }

    public function productList($id) {

        $products = Product::where('model_id',$id)->get();

        $params = [
            'products' => $products,
            'productTypes' => $this->productTypes,
        ];
        return view('product-list', $params)->withNav('product-list');
    }


    public function productDetails($id) {

        //$products = Product::where('id',$id)->get();
        $products = Product::find($id);
        $product_tracking_id = $products['tracking_id'];
        $product_picture = ProductPicture::where('product_id', $product_tracking_id)->first();
        $productAccessories = MobileAccessory::all();


        $project_path =  $this->getProjectUrl();

        $product_picture1= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'/thumbnail', $product_picture['product_picture_1']);
        $product_picture2= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'/thumbnail', $product_picture['product_picture_2']);
        $product_picture3= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'/thumbnail', $product_picture['product_picture_3']);

        $product_picture1_original= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'/original', $product_picture['product_picture_1']);
        $product_picture2_original= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'/original', $product_picture['product_picture_2']);
        $product_picture3_original= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'/original', $product_picture['product_picture_3']);

        $product_condition =  $products->condition['title'];
        $product_color =  $products->color['title'];
        $product_storage =  $products->storage['title'];
        $product_model =  $products->model['title'];
        //    echo "<pre>";
        //var_dump($products[0]);
        //dd($products[0]); die();
        //$objects = json_decode($product->accessories);
        // foreach ($products as $object) {
        //     //echo $object->accessories->title.", ";
        //     //  var_dump($object);
        //     // echo $object->accessories;
        //     $a = json_decode($object['accessories']);
        //
        //     // var_dump($a);
        //     $productAccessories = MobileAccessory::find($a);
        //
        //
        // } //die;

        $params = [
            'product'                            => $products,
            'productAccessories'                  => $productAccessories,
            'productTypes'                        => $this->productTypes,
            'ProductPicture'                      => $product_picture,
            'product_picture1'                    => $product_picture1,
            'product_picture2'                    => $product_picture2,
            'product_picture3'                    => $product_picture3,
            'product_picture1_original'           => $product_picture1_original,
            'product_picture2_original'           => $product_picture2_original,
            'product_picture3_original'           => $product_picture3_original,
            'product_model'                       => $product_model,
            'product_condition'                   => $product_condition,
            'product_color'                       => $product_color,
            'product_storage'                     => $product_storage,

        ];
        return view('product-details', $params)->withNav('product-details');
    }


    public function productSell(Request $request){

        $productTypes = ProductCategory::all();
        $params = [
            // 'route'  => route('product_lis'),
            'productTypes'  => $productTypes,
        ];

        return view('product-sell', $params)->withNav('product-sell');
    }

    public function productCategorySend(Request $request)
    {

        var_dump($request->all());
        die();
        $product_category = $request->product_category;

    }

    public function productListing(Request $request)
    {
        //   dd($request['product_category']);
        //       dd($request->all());
        $product = Product::all();
        $productCategory = $request['product_category'];
        $mobilecolor  = MobileColor::all();
        $mobileStorage  = MobileStorage::all();
        $mobileCondition  = MobileCondition::all();
        $mobileAccessory  = MobileAccessory::all();
        $productModel  = ProductModel::all();
        // var_dump($product);
        // die();
        $params = [
            'route'  => route('product_store'),
            'product'  => $product,
            'productTypes'  => $this->productTypes,
            'productCategory'  => $productCategory,
            'models' => $productModel,
            'colors' => $mobilecolor,
            'conditions' => $mobileCondition,
            'storages' => $mobileStorage,
            'accessories' => $mobileAccessory,
        ];
        return view('product-listing', $params)->withNav('product-listing')->withSubnav('product-listing');

    }


    public function productModelLoad(Request $request) {

        $id = isset($request->id) ? $request->id : "";
        $productmodels = ProductModel::where('product_category_id',$id)->get();
        if($productmodels) {
            $this->success();
            $this->message =[];
            $this->data = $productmodels;
        }
        return $this->output();
    }
    public function productAccessoryLoad(Request $request) {

        $id = isset($request->id) ? $request->id : "";
        $productAccessories = MobileAccessory::where('product_category_id',$id)->get();
        if($productAccessories) {
            $this->success();
            $this->message = [];
            $this->data = $productAccessories;
        }
        return $this->output();
    }

    public function mobileColors(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $colors_arr = MobileColor::whereColorId($id)->get();

        if($colors_arr){
            foreach ($colors_arr as $key => $color) {

                $color->title = $color->title;

                $colors[] = $color;

            }
            $this->success();
            $this->message = "Mobile Color records found!";
            $this->data = $colors;
        }
        return $this->output();
    }

    public function success(){
        $params = [

        ];
        return view('success.success', $params);
    }


    public function productListingEdit($product_id)
    {
        $product = Product::find($product_id);

        $product_tracking_id = $product['tracking_id'];
        $product_picture = ProductPicture::where('product_id', $product_tracking_id)->first();
        //    dd($product_picture);
        $productTypes = ProductCategory::all();
        $mobilecolor  = MobileColor::all();
        $mobileStorage  = MobileStorage::all();
        $mobileCondition  = MobileCondition::all();
        $mobileAccessory  = MobileAccessory::all();
        $productModel  = ProductModel::all();
        if($product){
            $title = 'Product Edit';
            $params = [
                'route'  => route('product_store'),
                'product' => $product,
                'productTypes' => $this->productTypes,
                'models' => $productModel,
                'colors' => $mobilecolor,
                'conditions' => $mobileCondition,
                'storages' => $mobileStorage,
                'accessories' => $mobileAccessory,
                'product_picture' => $product_picture,
                'product_tracking_id' => $product_tracking_id,
            ];
            return view('product.product-editing', $params);
        }
        return redirect()->route('product_editing')->withNav('home');
    }

    public function productStore(Request $request)
    {
        // var_dump($request->all());
        // die();

        $validator_data = [
            'title'      => 'required|max:500',
            //    'upload_file'   => 'required|mimes:png,jpg,jpeg,pdf|max:5120',//5MB
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please fill all the required field. ";
            return $this->validationError();
        }

        $sellerid = Auth::user()->id;



        if ($request->_ajax_id) {
            $product = Product::find($request->_ajax_id);
            $tracking_id = $product->tracking_id;
            $productPicture = ProductPicture::where('product_id',$tracking_id)->first();
            //    var_dump($request->picture_2);die();
        } else {
            $product = new Product;
            $productPicture = new ProductPicture;

            $tracking_id = $this->random_str(6);
            $product->privacy_policy = 'yes';
            $product->category_id = $request['product_category'];
            $product->email = $request->input('email');
            $product->imei = $request->input('imei');
            $product->tracking_id = $tracking_id;
            $productPicture->product_id = $tracking_id;
        }

        /* Save 'picture' */
        $thumbnail = ($request->hasFile('picture')) ? $this->uploadFiles($request->file('picture'),'thumb/') : "";
        $picture1 = ($request->hasFile('picture_1')) ? $this->uploadFiles($request->file('picture_1'),'picture/'.$tracking_id.'/') : "";
        $picture2 = ($request->hasFile('picture_2')) ? $this->uploadFiles($request->file('picture_2'),'picture/'.$tracking_id.'/') : "";
        $picture3 = ($request->hasFile('picture_3')) ? $this->uploadFiles($request->file('picture_3'),'picture/'.$tracking_id.'/') : "";


        $product->price = $request->input('price');
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->condition_id = $request->input('condition');
        $product->damage_description = $request->input('damage_description');
        $product->is_repaired = $request->input('is_repaired');
        $product->original_owner = $request->input('original_owner');
        $product->expire_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->input('date_expire'))));
        $product->is_locked = $request->input('is_locked');
        $product->is_rooted = $request->input('is_rooted');
        $product->mod_description = $request->input('mod_description');
        $product->accessories =isset($request->accessories) ? json_encode($request->accessories) : "";// $request->input('accessories_id');
        $product->accessories_description = $request->input('accessories_description');
        $product->shipping_international = $request->input('is_shipping');
        $product->shipping_country = $request->input('shipping_country');
        $product->shipping_location = $request->input('shipping_location');
        $product->storage_id = $request->input('storage_id');
        $product->color_id = $request->input('color_id');
        $product->model_id = $request->input('model_id');
        $product->paypal_confirmed = $request->input('paypal_confirm');
        $product->shipping_description = $request->input('shipping_description');
        $product->returns_allowed = $request->input('returns_allowed');
        $product->return_description = $request->input('return_description');
        $product->product_active = $request->input('product_active');
        $product->product_thumb = $thumbnail;
        $product->status = strtolower("verification-on-going");
        $product->user_id = $sellerid;

        if(strcmp($picture1,"")!=0){
            $productPicture->product_picture_1 = $picture1;
        }
        if(isset($request->picture_2)){

            $productPicture->product_picture_2 = $picture2;
        }
        if(strcmp($picture3,"")!=0){
            // var_dump($picture3);die();
            $productPicture->product_picture_3 = $picture3;
        }






        if ($product->save() && $productPicture->save()) {

            // if($product->email) {
            //     $emailService = new EmailService;
            //     $emailService->sendEmailWithTrackingID($product->id);
            // }

            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('success');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }


}
