<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use Illuminate\Http\Request;
use App\MobileAccessory;
use App\Http\Controllers\Controller;
use App\Models\Product\ProductCategory;
use App\Http\Datatables\AccessoryDatatable;

class AccessoryController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('product_accessory_index'),
            'dataload_url' => route('product_accessory_dataload'),
            'title' => "Product Accessory",
            'titles' => "Product Accessories",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "Category", "filter" => $this->filterText("category")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
        //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('product-accessory');
    }

    public function datatable(Request $request) {
        $ajax_table = new AccessoryDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $productcategories = ProductCategory::all();
        $accessory = new MobileAccessory;
        $title = 'Product Accessory';
        $params = [
            'accessory' => $accessory,
            'productcategories' => $productcategories,
            'route'    => route('product_accessory_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.product.product-accessory-create', $params)->withNav('product-accessory');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:255',
        ];

        $validator = Validator::make($request->all(), $validator_data);

        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        // $category = Category::find($request->_ajax_id);
        if ($request->_ajax_id) {
            $accessory = MobileAccessory::find($request->_ajax_id);
        } else {
            $accessory = new MobileAccessory;
        }

        $accessory->product_category_id = $request->input('product_category_id');
        $accessory->title = $request->input('title');
        $accessory->slug = $request->input('slug');

        if($accessory->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('product_accessory_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];

        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\MobileAccessory  $accessory
    * @return \Illuminate\Http\Response
    */
    public function show(Accessory $accessory)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\MobileAccessory  $accessory
    * @return \Illuminate\Http\Response
    */
    public function edit($accessory_id)
    {
        $accessory = MobileAccessory::find($accessory_id);
        if($accessory){
            $productcategories = ProductCategory::all();
            $title = 'Product Accessory';
            $params = [
                'accessory' => $accessory,
                'productcategories' => $productcategories,
                'route'    => route('product_accessory_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.product.product-accessory-create', $params);
        }
        return redirect()->route('product_accessory_index')->withNav('product-accessory');
    }


    public function productCategories(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $subcategories = MobileSubcategory::where('accessory_id', $id)->get();
        if($subcategories) {
            $this->success();
            $this->message = "records found!";
            $this->data = $subcategories;
        }
        return $this->output();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\MobileAccessory  $accessory
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Accessory $accessory)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\MobileAccessory  $accessory
    * @return \Illuminate\Http\Response
    */
    public function destroy(Accessory $accessory)
    {
        //
    }
}
