@component('mail::message')
<p>Congrats! Product Approved.</p>

<p>Hello {{$applicant_name}},<br> Your product succsfully verified. You can search your product via following tracking id. </p>

<p>Your Tracking ID : {{$tracking_id}}</p>
Thank you!<br>
{{ config('app.name') }}
@endcomponent
