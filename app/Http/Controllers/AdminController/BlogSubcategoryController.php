<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Validator;
use App\BlogCategory;
use App\BlogSubcategory;
use App\Http\Datatables\BlogSubcategoryDatatable;
use Illuminate\Http\Request;

class BlogSubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = [
            'base_url' => route('blog_subcategory_index'),
            'dataload_url' => route('blog_subcategory_dataload'),
            'title' => "Blog Subcategory",
            'titles' => "Blog Subcategories",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "Category", "filter" => $this->filterText("category")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
   //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('blogs')->withSubnav('blog-subcategories');
    }

    public function datatable(Request $request) {
        $ajax_table = new BlogSubcategoryDatatable;
        return $ajax_table->table($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategory = new BlogSubcategory;
        $categories  = BlogCategory::all();
        $title = 'subcategory';
        $project_url = $this->getProjectUrl();
        $params = [
            'categories' => $categories,
            'subcategory' => $subcategory,
            'route'    => route('blog_subcategory_store'),
            'method'   => 'post',
            'title'    => $title,
            'project_url'=> $project_url,
        ];
        return view('admin-pages.blog.blog-subcategory-create', $params)->withNav('blogs')->withSubnav('blog-subcategories');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator_data = [
            'category'     => 'required|max:255',
            'title'     => 'required|max:255',
            'slug'   => 'required|max:100|unique:subcategories,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Validaton Error!";
            return $this->validationError();
        }
    //    $file = ($request->hasFile('cover_photo')) ? $this->uploadFiles($request->file('cover_photo'),'subcategory_cover') : "";
        if ($request->_ajax_id) {
            $blogSubcategory = BlogSubcategory::find($request->_ajax_id);
        } else {
            $blogSubcategory = new BlogSubcategory;
        }


        $blogSubcategory->blogcategory_id = $request->input('category');
        $blogSubcategory->title = $request->input('title');
        $blogSubcategory->slug = $request->input('slug');
//        $subcategory->cover_photo = $file;
        if($blogSubcategory->save()){
            $this->success();
            $default = "";
            $this->message = "";
            $data['redirect_to'] = route('blog_subcategory_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlogSubcategory  $blogSubcategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogSubcategory $blogSubcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogSubcategory  $blogSubcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogSubcategory $blogSubcategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogSubcategory  $blogSubcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogSubcategory $blogSubcategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogSubcategory  $blogSubcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogSubcategory $blogSubcategory)
    {
        //
    }
}
