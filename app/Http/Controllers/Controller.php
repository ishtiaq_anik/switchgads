<?php

namespace App\Http\Controllers;

use App\Models\Product\ProductCategory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Components\Component;
use App\Http\Components\TableFilter;
use App\Http\Components\FileHandle;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use Component, TableFilter, FileHandle;

    protected $productTypes;

    public function __construct(){

        $this->productTypes = ProductCategory::all();

    }

}
