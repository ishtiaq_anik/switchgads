<section>
    <div class="c-content-box c-size-sm c-bg-white">
        <div class="container">
            <form class="c-shop-advanced-search-1">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control c-square c-theme input-lg" placeholder="Enter keywords or item number">
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"><i class="fa fa-search"></i>Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>	<!-- BEGIN: PAGE CONTENT -->
