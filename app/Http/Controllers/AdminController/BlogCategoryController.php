<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Validator;
use App\BlogCategory;
use App\BlogSubcategory;
use App\Http\Datatables\BlogCategoryDatatable;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('blog_category_index'),
            'dataload_url' => route('blog_category_dataload'),
            'title' => "Blog Category",
            'titles' => "Blog Categories",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
        //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('blogs')->withSubnav('blog-categories');
    }

    public function datatable(Request $request) {
        $ajax_table = new BlogCategoryDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $blogcategory = new BlogCategory;
        $title = 'Blog Category';
        $params = [
            'category' => $blogcategory,
            'route'    => route('blog_category_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.blog.blog-category-create', $params);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:255|unique:categories,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        // $category = Category::find($request->_ajax_id);
        if ($request->_ajax_id) {
            $blogcategory = BlogCategory::find($request->_ajax_id);
        } else {
            $blogcategory = new BlogCategory;
        }

        $blogcategory->title = $request->input('title');
        $blogcategory->slug = $request->input('slug');
        if($blogcategory->save()){
            $this->success();
            $default = "";
            $this->message = "";
            $data['redirect_to'] = route('blog_category_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\BlogCategory  $blogCategory
    * @return \Illuminate\Http\Response
    */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\BlogCategory  $blogCategory
    * @return \Illuminate\Http\Response
    */
    public function edit($blogcategory_id)
    {
        $blogcategory = BlogCategory::find($blogcategory_id);
        if($blogcategory){
            $title = 'Blog Category';
            $params = [
                'blogcategory' => $blogcategory,
                'route'    => route('blog_category_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.blog.blog-category-create', $params);
        }
        return redirect()->route('blog_category_index')->withNav('blogs');
    }


    public function subcategories(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $subcategories = BlogSubcategory::where('blogcategory_id', $id)->get();
        if($subcategories) {
            $this->success();
            $this->message = "records found!";
            $this->data = $subcategories;
        }
        return $this->output();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\BlogCategory  $blogCategory
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, BlogCategory $blogCategory)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\BlogCategory  $blogCategory
    * @return \Illuminate\Http\Response
    */
    public function destroy(BlogCategory $blogCategory)
    {
        //
    }
}
