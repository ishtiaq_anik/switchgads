@extends('layouts.master')

@section('page_style_plugin')
<!-- BEGIN: BASE PLUGINS  -->

<!-- END: BASE PLUGINS -->
@endsection

@section('content')

<div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-bold c-font-dark c-font-20 c-font-slim">About Us</h3>
			<h4 class="c-font-dark c-font-thin c-opacity-07">
				Page Sub Title Goes Here			</h4>
			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="#" class="c-font-dark">Pages</a></li>
				<li class="c-font-dark">/</li>
				<li><a href="page-about-1.html" class="c-font-dark">About Us 1</a></li>
				<li class="c-font-dark">/</li>
				<li class="c-state_active c-font-dark">Jango Components</li>

			</ul>
		</div>
	</div>
	<!-- BEGIN: PAGE CONTENT -->
	<!-- Begin: Content box -->
	<div class="c-content-box c-size-md">
		<div class="container">
			<!-- Begin: Cubeportfolio grid -->
			<div class="cbp-panel">

			</div>
		</div>
	</div>

	@endsection


	@section('page_script_plugin')


	<!-- BEGIN: PAGE SCRIPTS -->
	<script src="{{asset('assets/default/js/scripts/pages/extended-portfolio.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/frontend/js/custom.js')}}" type="text/javascript"></script>

	<!-- END: PAGE SCRIPTS -->
	@endsection
