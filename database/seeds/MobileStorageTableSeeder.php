<?php

use Illuminate\Database\Seeder;

class MobileStorageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $storage = [];
         for($i=0;$i<10;$i++){
             $storage[]=[
                 'title' => 'Storage' . $i
             ];

         }
         \App\MobileStorage::insert($storage);
     }
}
