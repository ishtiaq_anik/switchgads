<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Services\ProductService;
use App\Http\Datatables\IphoneListingDatatable;
use Illuminate\Http\Request;

class IphoneAdminController extends Controller
{
    public function iphoneListing()
    {
        $params = [
            'base_url' => route('iphone_index'),
            'dataload_url' => route('iphone_dataload'),
            'title' => "Iphone",
            'titles' => "Iphones",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Model", "filter" => $this->filterText("model")],
                [ "title" => "Status", "filter" => ""],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('listed-products')->withSubnav('iphones');
    }

    public function datatable(Request $request) {
        $ajax_table = new IphoneListingDatatable;
        return $ajax_table->table($request);
    }

    public function detail($product_id) {

        $product = Product::find($product_id);
    //    dd($product);

        if ($product) {
            //    $lang = $this->getLocale();
                $project_path =  $this->getProjectUrl();
                $url = url('');
                $picture_thumbnail = $this->storage_link($project_path, 'uploads/thumbnail', $product->product_thumb);
            //    dd($product->status);
            $product_statuses = $this->getProductStatus($product->status);
        //    dd($product_statuses['current']['specific_title']);
            $label = $product_statuses['current']['label'];
            $title = $product_statuses['current']['title'];


            $params = [
                'product'                              => $product,
                'product_number'                       => $product->id,
                'product_picture_1'                    => " ",// $this->storage_link($project_path, 'images/thumbnail', $product->picture),
                'product_picture_2'                    => " ",//$this->storage_link($project_path, 'images/thumbnail', $product->picture),
                'product_picture_3'                    => " ",//$this->storage_link($project_path, 'images/thumbnail', $product->picture),
                'product_thumbnail'                    => $picture_thumbnail,
                'status'                               => $product_statuses,//$this->getResidenceStatus($product->status),
                'label'                               => $label,//$this->getResidenceStatus($product->status),
                'title'                               => $title,//$this->getResidenceStatus($product->status),
            ];
            return view('admin-pages.product.products-detail', $params);
        }
        return redirect(route($this->nav));
    }





}
