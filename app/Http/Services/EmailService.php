<?php

namespace App\Http\Services;

use Mail;
//use App\Http\Components\InfobipAPI;
//use App\Http\Services\ProductService;
use App\Mail\EmailTrackingId;
use App\Mail\EmailForgetTrackingId;
use App\Mail\EmailApprove;
use App\Mail\EmailNotApprove;

class EmailService{
    protected $productService;

    public function sendEmailWithTrackingID($product_id){
        $id = (int)$product_id;
        $productService = new ProductService();
        $product = $productService->getProduct($product_id);
        if($product and $product->email){
            $to_email = $product->email;
            Mail::to($to_email)->send(new EmailTrackingId($id));
        }
    }
    public function sendApprovalEmail($product_id){
        $id = (int)$product_id;
        $productService = new ProductService();
        $product = $productService->getProduct($product_id);
        if($product and $product->email){
            $to_email = $product->email;
            Mail::to($to_email)->send(new EmailApprove($id));
        }
    }
    public function sendNotApprovalEmail($product_id){
        $id = (int)$product_id;
        $productService = new ProductService();
        $product = $productService->getProduct($product_id);
        if($product and $product->email){
            $to_email = $product->email;
            Mail::to($to_email)->send(new EmailNotApprove($id));
        }
    }

    public function sendEmailWithForgetTrackingID($product_id){
        $id = (int)$product_id;
        $productService = new ProductService();
        $product = $productService->getProduct($product_id);
        if($product and $product->email){
            $to_email = $product->email;
            Mail::to($to_email)->send(new EmailForgetTrackingId($id));
        }
    }
}
