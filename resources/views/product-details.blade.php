@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Product Details </h3>
                <h4 class=""></h4>
            </div>
            <!-- <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
            <li><a href="shop-product-details-2.html">Product Details 2</a></li>
            <li>/</li>
            <li class="c-state_active">Jango Components</li>

        </ul> -->
    </div>
</div>

</section>

<section>
    <div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
        <div class="container">
            <div class="c-shop-product-details-2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="c-product-gallery">
                            <div class="c-product-gallery-content">
                                <div class="c-zoom" img_order="1" style="position: relative; overflow: hidden;">
                                    <img src="{{$product_picture1_original}}">
                                    <img src="{{$product_picture1_original}}" class="zoomImg" style="position: absolute; top: -154px; left: -29.3919px; opacity: 0; width: 700px; height: 600px; border: none; max-width: none; max-height: none;">
                                </div>
                                <div class="c-zoom c-hide" img_order="2" style="position: relative; overflow: hidden;">
                                    <img src="{{$product_picture2_original}}">
                                    <img src="{{$product_picture2_original}}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 700px; height: 600px; border: none; max-width: none; max-height: none;">
                                </div>
                                <div class="c-zoom c-hide" img_order="3" style="position: relative; overflow: hidden;">
                                    <img src="{{$product_picture3_original}}">
                                    <img src="{{$product_picture3_original}}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 700px; height: 600px; border: none; max-width: none; max-height: none;">
                                </div>
                                <div class="c-zoom c-hide" img_order="4" style="position: relative; overflow: hidden;">
                                    <img src="{{asset('assets/img/content/shop3/90.jpg')}}">
                                    <img src="{{asset('assets/img/content/shop3/90.jpg')}}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 700px; height: 600px; border: none; max-width: none; max-height: none;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="c-product-meta">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">{{ $product['title'] }}</h3>
                                <div class="c-line-left"></div>
                            </div>

                            <div class="c-product-review">
                                <div class="c-product-rating">
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star-half-o c-font-green1"></i>
                                </div>
                                <div class="c-product-write-review">
                                    <a class="c-font-green1" href="#">Write a review</a>
                                </div>
                            </div>
                            <div class="c-product-price">${{ $product['price'] +5 }}</div>
                            <div class="c-product-short-desc">
                                {{ $product['description'] }}
                            </div>



                        </div>
                        <div class="c-product-gallery">

                            <div class="row c-product-gallery-thumbnail">
                                <div class="col-xs-3 c-product-thumb" style="height: 136px;">
                                    <img src="{{$product_picture1}}" img_order="1" width="120px" height="120px">
                                </div>
                                <div class="col-xs-3 c-product-thumb" style="height: 136px;">
                                    <img src="{{$product_picture2}}" img_order="2" width="120px" height="120px">
                                </div>
                                <div class="col-xs-3 c-product-thumb" style="height: 136px;">
                                    <img src="{{$product_picture3}}" img_order="3" width="120px" height="120px">
                                </div>
                                <div class="col-xs-3 c-product-thumb c-product-thumb-last" style="height: 136px;">
                                    <img src="{{asset('assets/img/content/shop3/90.jpg')}}" img_order="4" width="120px" height="120px">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="c-content-box c-size-md c-no-padding">
        <div class="c-shop-product-tab-1" role="tabpanel">
            <div class="container">
                <ul class="nav nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a class="c-font-uppercase c-font-bold" href="#tab-1" role="tab" data-toggle="tab">Description</a>
                    </li>
                    <li role="presentation">
                        <a class="c-font-uppercase c-font-bold" href="#tab-2" role="tab" data-toggle="tab">Additional Information</a>
                    </li>
                    <li role="presentation">
                        <a class="c-font-uppercase c-font-bold" href="#tab-3" role="tab" data-toggle="tab">Reviews (3)</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="tab-1">
                    <div class="c-product-desc ">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Product Info<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <p><span class="custom-title">Product Model :</span> {{ $product_model}}</p>
                                            <p><span class="custom-title">Product Color :</span> {{ $product_color}}</p>
                                            <p><span class="custom-title">Product Storage :</span> {{$product_storage }}</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Product Condition<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <p><span class="custom-title">Product Condition :</span> {{ $product_condition }}</p>
                                            <p><span class="custom-title">Product Repaired :</span> {{ $product['is_repaired'] }}</p>
                                            <p><span class="custom-title">Damage Description :</span> {{ $product['damage_description'] }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Shipping Info<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <p><span class="custom-title">Shipping International :</span> {{ $product['shipping_international'] }}</p>
                                            <p><span class="custom-title">Country :</span> {{ $product['shipping_country'] }}</p>
                                            <p><span class="custom-title">Location :</span> {{ $product['shipping_location'] }}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-2">
                    <div class="c-product-desc ">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Product Accessories<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <p><span class="custom-title">Product Accessories :</span>


                                            </p>
                                            <p>
                                                <span class="custom-title">Accessories Description :</span> {{ $product['accessories_description'] }}
                                            </p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Product Return Policy<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <p><span class="custom-title">Return Allowed :</span> {{ $product['returns_allowed'] }}</p>
                                            <p><span class="custom-title">Return Description :</span> {{ $product['return_description'] }}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-3">
                    <div class="container">
                        <h3 class="c-font-uppercase c-font-bold c-font-22 c-center c-margin-b-40 c-margin-t-40">Reviews for {{$product['title']}}</h3>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="c-user-avatar">
                                    <img src="{{asset('assets/img/content/avatars/team1.jpg')}}">
                                </div>
                                <div class="c-product-review-name">
                                    <h3 class="c-font-bold c-font-24 c-margin-b-5">Steve</h3>
                                    <p class="c-date c-theme-font c-font-14">July 4, 2015</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="c-product-rating c-right">
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star c-font-green1"></i>
                                    <i class="fa fa-star-half-o c-font-green1"></i>
                                </div>
                            </div>
                        </div>
                        <div class="c-product-review-content">
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer
                                adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud
                            </p>
                        </div>

                        <div class="row c-product-review-input">
                            <h3 class="c-font-bold c-font-uppercase">Submit your Review</h3>
                            <p class="c-review-rating-input">Rating:
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </p>
                            <textarea></textarea>
                            <button class="btn c-btn c-btn-square c-theme-btn c-font-bold c-font-uppercase c-font-white">Submit Review</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@section('page_script_three')

<!-- BEGIN: PAGE SCRIPTS -->
<script src="{{ asset('assets/plugins/zoom-master/jquery.zoom.min.js') }}" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->

@endsection
