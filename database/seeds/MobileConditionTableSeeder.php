<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Request;

class MobileConditionTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $type = ['Mint','Fair','Good', 'New' ];
        $condition = [];
        for($i=0;$i<4;$i++){
            $condition[]=[
                'title' => $type[$i]
            ];

        }
        \App\MobileCondition::insert($condition);
    }
}
