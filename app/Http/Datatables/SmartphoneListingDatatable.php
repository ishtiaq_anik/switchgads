<?php

namespace App\Http\Datatables;

use App\Product;
use App\Components;
use Auth;
use Illuminate\Http\Request;

class SmartphoneListingDatatable extends Datatable {

    private $nav = 'smartphone-listing';

    public function table(Request $request) {

        $this->deleteTableItem($request);
        $results = new Product;
        $title = isset($request->title) ? $request->title : null;
        $model = isset($request->model) ? $request->model_id : null;
        $status = isset($request->status) ? $request->status : null;
        $updated_from = isset($request->updated_from) ? $request->updated_from : null;
        $updated_to = isset($request->updated_to) ? $request->updated_to : null;

        $results = $results->where('category_id', '2');

        if ($title) {
            $results = $results->where('title', 'like', '%' . $title . '%');
        }
        if ($model) {
            $results = $results->where('model', 'like', '%' . $model . '%');
        }
        if ($updated_from) {
            $results = $results->where('updated_at', '>=', $this->date_filter($updated_from));
        }
        if ($updated_to) {
            $results = $results->where('updated_at', '<=', $this->date_filter($updated_to, true));
        }
        $tableColumns = [
            "",
            "title",
            "model",
            "status",
            "updated_at",
            ""
        ];
        $sortColumn = $request->order[0]['column'];
        $sortDir = $request->order[0]['dir'];
        $sort_field = $tableColumns[$sortColumn];

        switch ($sort_field) {
            case "title":
            $results = $results->orderBy('type', $sortDir);
            break;
            case "model":
            $results = $results->orderBy('model', $sortDir);
            break;
            case "status":
            $results = $results->orderBy('status', $sortDir);
            break;
            case "updated_at":
            $results = $results->orderBy('updated_at', $sortDir);
            break;
        }

        $results = $results->get();

        $iTotalRecords = $results->count();
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $data = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $result = $results[$i];

            //$edit = "<a class='btn btn-sm btn-outline blue table-row-edit' title='Edit' href='".route('mobile_category_edit', $result->id)."' data-id='" . $result->id . "'> <i class='icon-note'></i>Edit</a>";
            $detail = "<a class='btn btn-sm btn-outline blue '  href='".route('product_detail',[$result->id])."' title='Detail'> <i class='icon-note'></i> Detail</a>";
            $delete = "<a class='btn btn-sm btn-outline red table-row-delete' title='Remove' href='javascript:;' data-id='" . $result->id . "'> <i class='icon-trash'></i>Delete</a>";
            $data[] = [
                $i+1,
                isset($result->title) ? $result->title : "",
                isset($result->model) ? $result->model->title : "",
                isset($result->status)? $this->getProductStatusesForDatatableArray($result->status) : "",
                isset($result->updated_at) ? date_format($result->updated_at, 'd/m/Y') : "",
                (isset($detail) ? $detail : "") . (isset($delete) ? $delete : ""),
            ];
        }
        $this->data = $data;
        $this->draw = $sEcho;
        $this->total = $iTotalRecords;
        $this->filtered = $iTotalRecords;
        return $this->outputDatatable();
    }

    public function deleteTableItem($request) {
        if (isset($request->actionType) && $request->actionType == "delete_action") {
            $mobilecategory = Product::find($request->record_id);
            if ($mobilecategory) {
                $mobilecategory->delete();
                $this->status = "OK";
                $this->message = "Product deleted successfully";
            } else {
                $this->message = "Product delete failed";
            }
        }
    }

}
