@extends('layouts.master')

@section('page_style_plugin')
<!-- BEGIN: BASE PLUGINS  -->

<!-- END: BASE PLUGINS -->
@endsection

@section('content')

<div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-bold c-font-dark c-font-20 c-font-slim">About Us</h3>
			<h4 class="c-font-dark c-font-thin c-opacity-07">
				Page Sub Title Goes Here			</h4>
			</div>
			<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
				<li><a href="#" class="c-font-dark">Pages</a></li>
				<li class="c-font-dark">/</li>
				<li><a href="page-about-1.html" class="c-font-dark">About Us 1</a></li>
				<li class="c-font-dark">/</li>
				<li class="c-state_active c-font-dark">Jango Components</li>

			</ul>
		</div>
	</div>
	<!-- BEGIN: PAGE CONTENT -->
	<!-- Begin: Content box -->
	<div class="c-content-box c-size-md">
		<div class="container">
			<!-- Begin: Cubeportfolio grid -->
			<div class="cbp-panel">
				<div id="filters-container" class="cbp-l-filters-buttonCenter">
					<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
						All <div class="cbp-filter-counter"></div>
					</div>
					<div data-filter=".identity" class="cbp-filter-item">
						Identity <div class="cbp-filter-counter"></div>
					</div>
					<div data-filter=".web-design" class="cbp-filter-item">
						Web Design <div class="cbp-filter-counter"></div>
					</div>
					<div data-filter=".graphic" class="cbp-filter-item">
						Graphic <div class="cbp-filter-counter"></div>
					</div>
					<div data-filter=".logos" class="cbp-filter-item">
						Logo <div class="cbp-filter-counter"></div>
					</div>
					<div data-filter=".logos, .graphic" class="cbp-filter-item">
						Logo & Graphic <div class="cbp-filter-counter"></div>
					</div>
				</div>

				<div id="grid-container" class="cbp cbp-l-grid-masonry-projects">
					@foreach($photos as $photo)
					<div class="cbp-item graphic">
						<div class="cbp-caption">
							<div class="cbp-caption-defaultWrap">
								<img src="{{$project_url}}/storage/uploads/original/{{$photo->photo_link}}" alt="">
							</div>
							<div class="cbp-caption-activeWrap">
								<div class="c-masonry-border"></div>
								<div class="cbp-l-caption-alignCenter">
									<div class="cbp-l-caption-body">
										<a href="ajax/projects/project1.html?test=1" class="cbp-singlePage cbp-l-caption-buttonLeft btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase">Explore</a>
										<a href="{{$project_url}}/storage/uploads/original/{{$photo->photo_link}}" class="cbp-lightbox cbp-l-caption-buttonRight btn c-btn-square c-btn-border-1x c-btn-white c-btn-bold c-btn-uppercase" data-title="{{$photo->title}}<br>{{$photo->descripton}}">Zoom</a>
									</div>
								</div>
							</div>
						</div>
						<a href="ajax/projects/project1.html" class="cbp-singlePage cbp-l-grid-masonry-projects-title">{{$photo->title}}</a>
						<div class="cbp-l-grid-masonry-projects-desc">
							<a href="javascript:;" data-target="{{$photo->id}}" class="like" data-action="">
								<span id="likes{{$photo->id}}">{{$photo->likes}}</span> <span aria-hidden="true" class="icon-star"></span>

							</a>
						</div>
					</div>
					@endforeach


				</div>

				<div id="loadMore-container" class="cbp-l-loadMore-button c-margin-t-60">
					<a href="ajax/extended-portfolio/load-more.html" class="cbp-l-loadMore-link btn c-btn-square c-btn-border-2x c-btn-dark c-btn-bold c-btn-uppercase">
						<span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
						<span class="cbp-l-loadMore-loadingText">LOADING...</span>
						<span class="cbp-l-loadMore-noMoreLoading">NO MORE PHOTOS</span>
					</a>
				</div>
			</div>
			<!-- End-->
		</div>
	</div>

	@endsection


	@section('page_script_plugin')


	<!-- BEGIN: PAGE SCRIPTS -->
	<script src="{{asset('assets/default/js/scripts/pages/extended-portfolio.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/frontend/js/custom.js')}}" type="text/javascript"></script>

	<!-- END: PAGE SCRIPTS -->
	@endsection
