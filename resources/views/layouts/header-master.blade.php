<?php
$nav = isset($nav) ? $nav : ""; $subnav = isset($subnav) ? $subnav : "";

?>

<!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
<!-- BEGIN: HEADER -->
<header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">
    <div class="c-navbar">
        <div class="container">
            <!-- BEGIN: BRAND -->
            <div class="c-navbar-wrapper clearfix">
                <div class="c-brand c-pull-left">
                    <a href="index.html" class="c-logo">
                        <img src="{{asset('assets/img/content/logos/logo-3.png' )}} " alt="switchgads" class="c-desktop-logo">
                        <img src="{{asset('assets/img/content/logos/logo-3.png' )}}" alt="switchgads" class="c-desktop-logo-inverse">
                        <img src="{{asset('assets/img/content/logos/logo-3.png' )}}" alt="switchgads" class="c-mobile-logo"> </a>
                        <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                            <span class="c-line"></span>
                            <span class="c-line"></span>
                            <span class="c-line"></span>
                        </button>
                        <button class="c-topbar-toggler" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <button class="c-search-toggler" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                        <button class="c-cart-toggler" type="button">
                            <i class="icon-handbag"></i>
                            <span class="c-cart-number c-theme-bg">2</span>
                        </button>
                    </div>
                    <!-- END: BRAND -->
                    <!-- BEGIN: QUICK SEARCH -->
                    <form class="c-quick-search" action="#">
                        <input type="text" name="query" placeholder="Type to search..." value="" class="form-control" autocomplete="off">
                        <span class="c-theme-link">&times;</span>
                    </form>
                    <!-- END: QUICK SEARCH -->
                    <!-- BEGIN: HOR NAV -->
                    <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
                    <!-- BEGIN: MEGA MENU -->
                    <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                    <nav class="c-mega-menu c-pull-right c-fonts-uppercase c-fonts-bold">
                        <ul class="nav navbar-nav c-theme-nav">
                            <li>
                                <a href="{{ route('userhome') }}" class="c-link  {{ ($nav == 'home') ? 'c-active' : '' }}">Home
                                    <span class="c-arrow c-toggler"></span>
                                </a>

                            </li>
                            @if(!empty($productTypes))
                            @foreach($productTypes as $category)
                            <li class="c-menu-type-classic {{ ($nav == $category->id) ? 'c-active' : '' }}">
                                <a href="{{ route('product_categories_show',$category->id) }}" class="c-link dropdown-toggle">{{$category->title}}<span class="c-arrow c-toggler"></span></a>
                            </li>

                            @endforeach
                            @endif
                            <li>
                                <a href="{{ route('product_sell') }}" class=" c-btn btn-no-focus c-btn-header btn btn-sm c-btn-green  c-btn-circle c-btn-uppercase c-btn-sbold">Sell</a>
                            </li>

                            @if(\Illuminate\Support\Facades\Auth::check())
                            <li>
                                <a class="c-link" href="#" ><img class="c-header-img" src="{{asset('assets/base/img/content/stock/9.jpg')}}" alt="" class="img-responsive"></a>
                                <ul class="dropdown-menu c-menu-type-classic c-pull-left">

                                    <li class="dropdown-submenu c-bg-grey">
                                        <a href="{{route('user_profile_dashboard')}}">Profile<span class="c-arrow c-toggler"></span></a>

                                    </li>
                                    <li class="dropdown-submenu c-bg-grey">
                                        <a  href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">logout</a>


                                    </li>

                                </ul>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>


                            @else
                            <li>
                                <a href="javascript:;" data-toggle="modal" data-target="#login-form" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold"><i class="icon-user"></i> Sign In</a>
                            </li>
                            @endif

                        </ul>
                    </nav>
                    <!-- END: MEGA MENU -->
                    <!-- END: LAYOUT/HEADERS/MEGA-MENU -->
                    <!-- END: HOR NAV -->
                </div>
                <!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->

                <!-- END: LAYOUT/HEADERS/QUICK-CART -->
            </div>
        </div>
    </header>
    <!-- END: HEADER -->
    <!-- END: LAYOUT/HEADERS/HEADER-1 -->
