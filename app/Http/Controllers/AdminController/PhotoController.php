<?php

namespace App\Http\Controllers\AdminController;


use Validator;
use App\Photo;
use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Datatables\PhotoDatatable;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = [
            'base_url' => route('photo_index'),
            'dataload_url' => route('photo_dataload'),
            'title' => "Photo",
            'titles' => "Photos",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Thumb", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "Category", "filter" => $this->filterText("category")],
                [ "title" => "Subcategory", "filter" => $this->filterText("subcategory")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
       // dd($params);
        return view('admin.table', $params)->withNav('photos')->withSubnav('upload');
    }

    public function datatable(Request $request) {
        $ajax_table = new PhotoDatatable;
        return $ajax_table->table($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $photo = new Photo;
        $categories  = Category::all();
        $subcategories  = Subcategory::all();
        $title = 'photo';
        $params = [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'photo' => $photo,
            'route'    => route('photo_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.photos.photo-create', $params)->withNav('photos')->withSubnav('upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator_data = [
            'title'      => 'required|max:500',
            'slug'     => 'required|max:500',
            'upload_file'   => 'required|mimes:png,jpg,jpeg,pdf|max:5120',//5MB
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please fill all the required field. ";
            return $this->validationError();
        }
        $file = ($request->hasFile('upload_file')) ? $this->uploadFiles($request->file('upload_file'),'uploads') : "";
        $photo = new Photo;
        $photo->title= $request->input('title');
        $photo->slug = $request->input('slug');
        $photo->category_id = $request->input('category');
        $photo->subcategory_id = $request->input('category');
        $photo->description = $request->input('description');
        $photo->photo_link= $file;
        if($photo->save()){
            $this->success();
            $default = "";
            $this->message =[];
            $data['redirect_to'] = route('photo_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        //
    }
}
