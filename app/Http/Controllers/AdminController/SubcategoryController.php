<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Subcategory;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Datatables\SubcategoryDatatable;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = [
            'base_url' => route('subcategory_index'),
            'dataload_url' => route('subcategory_dataload'),
            'title' => "Subcategory",
            'titles' => "Subcategories",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "Category", "filter" => $this->filterText("category")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
       // dd($params);
        return view('admin.table', $params)->withNav('photos')->withSubnav('subcategories');
    }

    public function datatable(Request $request) {
        $ajax_table = new SubcategoryDatatable;
        return $ajax_table->table($request);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategory = new Subcategory;
        $categories  = Category::all();
        $title = 'subcategory';
        $project_url = $this->getProjectUrl();
        $params = [
            'categories' => $categories,
            'subcategory' => $subcategory,
            'route'    => route('subcategory_store'),
            'method'   => 'post',
            'title'    => $title,
            'project_url'=> $project_url,
        ];
        return view('admin-pages.photos.subcategory-create', $params)->withNav('photos')->withSubnav('subcategories');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator_data = [
            'category'     => 'required|max:255',
            'title'     => 'required|max:255',
            'slug'   => 'required|max:100|unique:subcategories,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Validaton Error!";
            return $this->validationError();
        }
        $file = ($request->hasFile('cover_photo')) ? $this->uploadFiles($request->file('cover_photo'),'subcategory_cover') : "";
        if ($request->_ajax_id) {
            $subcategory = Subcategory::find($request->_ajax_id);
        } else {
            $subcategory = new Subcategory;
        }


        $subcategory->category_id = $request->input('category');
        $subcategory->title = $request->input('title');
        $subcategory->slug = $request->input('slug');
        $subcategory->cover_photo = $file;
        if($subcategory->save()){
            $this->success();
            $default = "";
            $this->message = "";
            $data['redirect_to'] = route('subcategory_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategory $subcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function edit($subcategory_id)
    {
        $subcategory = Subcategory::find($subcategory_id);
        $categories  = Category::all();
        $project_url = $this->getProjectUrl();
        if($subcategory){
            $title = 'subcategory';
            $params = [
                'categories' => $categories,
                'subcategory' => $subcategory,
                'route'    => route('subcategory_store'),
                'method'   => 'post',
                'title'    => $title,
                'project_url'=> $project_url,
            ];
            return view('admin-pages.photos.subcategory-create', $params)->withNav('photos')->withSubnav('subcategories');
        }
        return redirect()->route('subcategory_index')->withNav('photos')->withSubnav('subcategories');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        //
    }
}
