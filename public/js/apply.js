/**********************  Number field varifier **********************/
$('input[name="year"]').keyup(function () {
    var val = $(this).val();
    var max_size = 4;
    $('input[name="year"]').val(validateInputAsNumber(val, max_size));
});

$('#category').on('change', function() {
    if($(this).find(":selected").val()=="0"){
        $('#number-addon').html("@");
    }
    else{
        $('#number-addon').html($(this).find(":selected").val());
    }
});

function checkFileExtension(input) {
    var extension = input.split('.').pop().toLowerCase(); // image/jpg, image/png, image/jpeg...
    // allow jpg, png, jpeg, bmp, gif,
    if ($.inArray(extension, ['gif', 'png', 'jpg', 'jpeg', 'bmp', 'pdf']) == -1) {
        return "";
    }
    return extension;
}

/********************************* For Preview ***************************************/
function successAjaxForm(response) {
    validationCheck(response);
    if (response.success) {
        $(".form-group").removeClass("has-error").children("div").children(".help-block").remove();
        if (response.data.redirect_to) {
            window.location.replace(response.data.redirect_to);
        }
        preview();
        $('.bs-example-modal-lg').modal('show');
    } else {
        appAlert(response.message, 'danger');
    }
}
function failedAjaxForm(response) {
    appAlert(response.message, "danger");
}
function appAlert(message, status) {
    var container = ".c-content-panel .c-body";
    var place = "prepend";
    status = (status !== 'undefined') ? status : 'danger';
    // alert(message);
    App.alert({
        type: status,
        icon: status,
        message: message,
        container: container,
        place: place
    });
}
$('#ajaxFormMain').on("submit", function (e) {

    e.preventDefault();
    let form_data = new FormData(this);
    let form = $(this);
    form_data.url = form.children('input[name="_action"]').val();
    form_data.method = form.children('input[name="_method"]').val();
    form_data.preview = window.prefiew;
    if (!window.prefiew) {
        console.log(form_data);
        // if (typeof $('#ajaxFormMain').children('input[name="tracking_id"]').val() != 'undefined') {
        //     form_data.url = form_data.url.replace("/certificate/update", "/apply/validate");
        // } else {
        //     form_data.url = form_data.url.replace("/store", "/validate");
        // }
    }
    callFormAjax(form_data, 'successAjaxForm:failedAjaxForm');
});
