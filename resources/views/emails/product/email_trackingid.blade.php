@component('mail::message')
<p>Product Listing</p>

<p>Hello {{$applicant_name}},<br> This is to inform you that your request for new product listing successfully done. You will be notified soon after the product verification.</p>

Thank you!<br>
{{ config('app.name') }}
@endcomponent
