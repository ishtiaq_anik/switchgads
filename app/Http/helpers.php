<?php
use Illuminate\Support\HtmlString;

if (! function_exists('ajax_action')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_action($action = "/")
     {
         return new HtmlString('<input type="hidden" name="_action" value="'.$action.'">');
     }
}

if (! function_exists('ajax_method')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_method($method = "post")
     {
         return new HtmlString('<input type="hidden" name="_method" value="'.$method.'">');
     }
}

if (! function_exists('ajax_success')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_success($successFunction = "successAjaxForm")
     {
         return new HtmlString('<input type="hidden" name="_success" value="'.$successFunction.'">');
     }
}

if (! function_exists('ajax_failed')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_failed($failedFunction = "failedAjaxForm")
     {
         return new HtmlString('<input type="hidden" name="_failed" value="'.$failedFunction.'">');
     }
}

if (! function_exists('ajax_id')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_id($ajax_id = null)
     {
         return new HtmlString('<input type="hidden" name="_ajax_id" value="'.$ajax_id.'">');
     }
}
