@extends('layouts.master')

@section('title', 'Product Sell')

@section('page_style_one')
<link href="{{ asset('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Product Details 2</h3>
                <h4 class="">Page Sub Title Goes Here</h4>

            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="shop-product-details-2.html">Product Details 2</a></li>
                <li>/</li>
                <li class="c-state_active">Jango Components</li>

            </ul>
        </div>
    </div>

</section>
<section>
    <div class="c-content-box c-size-md c-bg-white">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <form action="{{ route('product_listing')}}" method="post" class="form-horizontal" enctype='multipart/form-data'>

                        {{ csrf_field() }}

                        <!-- payment Info -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label">basic controls</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Want To sell?</h4>
                                    <p class="c-font-thin c-font-14">Tell us what! We will help you.</p>
                                </div>
                                <div class="form-group has-error-product_category has-error-has-success">
                                    <div class="col-md-10">
                                        <select class="form-control  c-square c-theme"  name="product_category">
                                            @foreach($productTypes as $product)
                                            <option value="{{ $product->id }}" >
                                                {{ $product->title }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Terms & Conditions</h4>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-12">
                                        <ul class="c-content-list-1 c-theme c-separator-dot c-square">
                                            <li class="c-bg-before-theme"> The device I am listing meets all the Criteria for Sale AND I agree to the Other Terms (below).</li>
                                            <li class="c-bg-before-theme">I accept the Terms of Use for this site and service.</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group c-margin-t-40">
                                    <div class="col-sm-offset-5 col-md-7">
                                        <button type="submit" id="form-submit" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                                        <!-- <a href="{{route('product_listing')}}" class="btn btn-md c-btn-red c-btn-square"><i class="fa fa-bell-o"></i> JANGO</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

                <div class="col-md-6">
                    <div class="c-content-panel  c-panel-border">
                        <div class="c-label">basic controls</div>
                        <div class="c-body">
                            <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                <h4 class="c-left c-font-uppercase c-font-bold">Required</h4>

                            </div>
                            <ul class="c-content-list-1 c-theme c-separator-dot">
                                <li class="c-bg-before-green">Device Should be fully functional, including all ports and buttons.</li>
                                <li class="c-bg-before-green">Device ready for activation, should not be lost or stolen.</li>
                                <li class="c-bg-before-green">Device should has functional battery without any damage.</li>
                                <li class="c-bg-before-green">Seller must agree the terms & conditions.</li>
                            </ul>
                            <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                <h4 class="c-left c-font-uppercase c-font-bold">Not Allowed</h4>

                            </div>
                            <ul class="c-content-list-1 c-theme c-separator-dot">
                                <li class="c-bg-before-red">Device with bad ESN.</li>
                                <li class="c-bg-before-red">Cracks on device screen or glass.</li>
                                <li class="c-bg-before-red">Device with water damage.</li>
                                <li class="c-bg-before-red">Devices with an outstanding EIP balance.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>

</section>
@endsection

@section('page_script_two')

<script src="{{ asset('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<!-- <script src="{{ asset('assets/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script> -->
@endsection
@section('page_script_three')
<script src="{{ asset('assets/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/js/datepicker.js') }}" type="text/javascript"></script>

<script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
<!-- <script src="{{ asset('assets/admin/pages/scripts/profile.min.js') }}" type="text/javascript"></script> -->

<script src="{{asset('js/apply.js')}}" type="text/javascript"></script>
@endsection
