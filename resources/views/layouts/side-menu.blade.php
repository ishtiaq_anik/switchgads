<div class="c-layout-sidebar-menu c-theme ">
    <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
    <div class="c-sidebar-menu-toggler">
        <h3 class="c-title c-font-uppercase c-font-bold">My Profile</h3>
        <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
            <span class="c-line"></span> <span class="c-line"></span> <span class="c-line"></span>
        </a>
    </div>

    <ul class="c-sidebar-menu collapse " id="sidebar-menu-1">
        <li class="c-dropdown c-open">
            <a href="javascript:;" class="c-toggler">My Profile<span class="c-arrow"></span></a>
            <ul class="c-dropdown-menu">
                <li class="{{ ($sidenav == 'dashboard') ? 'c-active' : '' }}">
                    <a  href="{{route('user_profile_dashboard')}}">My Dashbord</a>
                </li>
                <li class="{{ ($sidenav == 'profile') ? 'c-active' : '' }}">
                    <a  href="{{route('user_profile_edit')}}">Edit Profile</a>
                </li>
                <li class="{{ ($sidenav == 'history') ? 'c-active' : '' }}">
                    <a href="{{route('user_selling_history')}}">Selling History</a>
                </li>
            </ul>
        </li>
    </ul><!-- END: LAYOUT/SIDEBARS/SHOP-SIDEBAR-DASHBOARD -->
</div>
