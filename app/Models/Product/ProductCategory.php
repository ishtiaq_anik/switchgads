<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{

        use SoftDeletes;

        protected $dates = ['deleted_at'];

        public function models()
        {
            return $this->hasMany('App\Models\Product\ProductModel', 'product_category_id');
        }
}
