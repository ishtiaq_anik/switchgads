<?php

namespace App\Http\Controllers;

use App\Home;
use App\Photo;
use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use Mbarwick83\Instagram\Instagram;

class HomeController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //    dd('all');
        $photos = Photo::orderBy('likes', 'desc')->get();
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $project_url= $this->getProjectUrl();
        $params = [
            'photos' => $photos,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'project_url'=> $project_url,

        ];


        return view('home', $params)->withNav("home");
    }
    public function instaShow()
    {
        return view('home', $params)->withNav("home");
    }

    public function instaLogin(Instagram $instagram)
    {
        dd($instagram);
        //    https://api.instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=code
        //    dd($instagram);
        return $instagram->getLoginUrl();
        // or Instagram::getLoginUrl();
    }

    // Get access token on callback, once user has authorized via above method
    public function callback(Request $request, Instagram $instagram)
    {
        $response = $instagram->getAccessToken($request->code);
        // or $response = Instagram::getAccessToken($request->code);

        if (isset($response['code']) == 400)
        {
            throw new \Exception($response['error_message'], 400);
        }

        return $response['access_token'];
    }


    public function subcategoriesShow($slug)
    {
        $subcategories = Subcategory::where('slug', $slug)->first();

        $photos = $subcategories->photos;
        $categories = $this->getCategory();
        $project_url= $this->getProjectUrl();
        $params = [

            'photos' => $photos,
            'project_url'=> $project_url,
            'categories' => $categories,

        ];


        //return view('subcategories', $params);
        return view('subcategories-show',$params);
    }
    public function blogListShow()
    {
        //    $subcategories = Subcategory::where('slug', $slug)->first();

        //    $photos = $subcategories->photos;
        $categories = $this->getCategory();
        $project_url= $this->getProjectUrl();
        $params = [

            //    'photos' => $photos,
            'project_url'=> $project_url,
            'categories' => $categories,

        ];


        //return view('subcategories', $params);
        return view('blog.blog-list',$params)->withNav('blog');
    }
    public function tipsGridShow()
    {
        //    $subcategories = Subcategory::where('slug', $slug)->first();

        //    $photos = $subcategories->photos;
        $categories = $this->getCategory();
        $project_url= $this->getProjectUrl();
        $params = [

            //    'photos' => $photos,
            'project_url'=> $project_url,
            'categories' => $categories,

        ];


        //return view('subcategories', $params);
        return view('tips.tips-grid',$params)->withNav('tips');
    }
    public function componentShow()
    {
        //    $subcategories = Subcategory::where('slug', $slug)->first();

        //    $photos = $subcategories->photos;
        $categories = $this->getCategory();
        $project_url= $this->getProjectUrl();
        $params = [

            //    'photos' => $photos,
            'project_url'=> $project_url,
            'categories' => $categories,

        ];


        //return view('subcategories', $params);
        return view('components.component-show',$params)->withNav('components');
    }

    public function likes($id){
        $photo = Photo::find($id);
        $photo->likes = $photo->likes+1;
        if($photo->save()){
            $this->success();
            $default = "";
            $this->message = $photo->likes;
            $data['likes'] =  $photo->likes;
            $data['id'] =  $photo->id;
            $data['redirect_to'] = "";
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

}
