@extends('layouts.master')

@section('page_style_plugin')
<!-- BEGIN: BASE PLUGINS  -->

<!-- END: BASE PLUGINS -->
@endsection

@section('content')

<div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-bold c-font-dark c-font-20 c-font-slim">{{$nav}}</h3>
			<h4 class="c-font-dark c-font-thin c-opacity-07">
				Page Sub Title Goes Here
			</h4>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
			<li><a href="#" class="c-font-dark">Pages</a></li>
			<li class="c-font-dark">/</li>
			<li><a href="page-about-1.html" class="c-font-dark">About Us 1</a></li>
			<li class="c-font-dark">/</li>
			<li class="c-state_active c-font-dark">Jango Components</li>

		</ul>
	</div>
</div>


<!-- BEGIN: CONTENT/stockS/stock-2-3 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space" style="background-image: url(assets/base/img/content/backgrounds/34.jpg)">
	<div class="container">
	<div class="c-content-title-1">
		<h3 class="c-font-uppercase c-center c-font-bold">Most Popular</h3>
		<div class="c-line-center c-theme-bg"></div>
	</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(assets/base/img/content/stock/06.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-top-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(assets/base/img/content/stock/07.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">New Fashion</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(assets/base/img/content/stock/20.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Latest Trends</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 330px; background-image: url(assets/base/img/content/stock/17.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Modern</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/stockS/stock-2-3 -->

<!-- BEGIN: CONTENT/stockS/stock-1-2 -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="c-content-tab-5 c-theme">
			<!-- Nav tabs -->
			<ul class="nav nav-pills c-nav-tab c-arrow" role="tablist">
				<li role="presentation" class="active">
					<a class="c-font-uppercase" href="#watches2" aria-controls="watches" role="tab" data-toggle="tab">Watches</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#phone2" aria-controls="phone" role="tab" data-toggle="tab">Phone</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#imac2" aria-controls="imac" role="tab" data-toggle="tab">iMac</a>
				</li>
				<li role="presentation">
					<a class="c-font-uppercase" href="#accessories2" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="watches2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/69.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/70.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/79.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="phone2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/59.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/71.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="imac2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/73.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/74.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/91.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="accessories2">
					<div class="row">
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/63.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/67.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="c-content c-content-overlay">
								<div class="c-overlay-wrapper c-overlay-padding">
									<div class="c-overlay-content">
										<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
									</div>
								</div>
								<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/77.jpg);"></div>
								<div class="c-overlay-border"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/stockS/stock-1-2 -->

<!-- BEGIN: CONTENT/stockS/stock-4-1 -->
<div class="c-content-box c-no-padding c-overflow-hide c-bs-grid-reset-space">
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="c-content-product-4 c-content-bg-1">
				<div class="col-md-6 col-sm-6">
					<div class="c-wrapper">
						<div class="c-side-image" style="background-image: url(assets/base/img/content/stock/32.jpg);"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-content c-align-right">
						<h3 class="c-title c-font-bold c-font-35 c-font-dark">iPhone 6</h3>
						<p class="c-description c-font-20 c-font-regular">Lorem ipsum dolor sit amet, adipiscing elit coectetuer adipiscing elit adipiscing consectetuer.</p>
						<p class="c-price c-font-60 c-font-thin c-font-dark">$649</p>
						<a href="shop-product-details-2.html" class="btn btn-lg c-btn-grey-3 c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="c-content-product-4 c-content-bg-2">
				<div class="col-md-6 col-sm-6">
					<div class="c-content c-align-left">
						<h3 class="c-title c-font-bold c-font-35 c-font-dark">360 Watch</h3>
						<p class="c-description c-font-20 c-font-regular">Lorem ipsum dolor sit amet, adipiscing elit coectetuer adipiscing elit adipiscing consectetuer.</p>
						<p class="c-price c-font-60 c-font-thin c-font-dark">$649</p>
						<a href="shop-product-details-2.html" class="btn btn-lg c-btn-grey-3 c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-wrapper">
						<div class="c-side-image" style="background-image: url(assets/base/img/content/stock/32.jpg);"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/stockS/stock-4-1 -->

<!-- BEGIN: CONTENT/stockS/stock-5-1 -->
<div class="c-content-box c-overflow-hide c-bs-grid-reset-space">
	<div class="row">
		<div class="col-md-6">
			<div class="c-content-product-5">
				<div class="c-bg-img-center" style="height:800px;background-image: url(assets/base/img/content/stock/32.jpg)">
					<div class="c-detail c-bg-dark c-bg-opacity-2">
						<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
						<h3 class="c-title c-font-uppercase c-font-bold c-font-white c-font-90">Promo</h3>
						<p class="c-desc c-font-white c-font-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam dolore</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class="c-font-thin">Watch</span><br/>Collection
								</h3>
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 400px; background-image: url(assets/base/img/content/stock/59.jpg);"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="c-content-product-5 c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<h3 class="c-title c-margin-tb-30 c-font-uppercase c-font-bold c-font-30 c-font-white c-padding-20">
									<span class="c-font-thin">Sunglasses</span><br/>Collection
								</h3>
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Shop Now</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 400px; background-image: url(assets/base/img/content/stock/26.jpg);"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="c-content-product-5">
						<div class="c-bg-img-center c-center" style="height:400px;background-image: url(assets/base/img/content/stock/05.jpg)">
							<div class="c-wrapper c-center-vertical">
								<h3 class="c-title c-margin-tb-30 c-font-30 c-font-uppercase c-font-bold c-font-white">
									<span class="c-line"><span class="c-font-thin">New</span> Collection</span>
								</h3>
								<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square c-margin-t-20">Shop Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/stockS/stock-5-1 -->

<!-- BEGIN: CONTENT/MISC/PROMO-1-3 -->
<div class="c-content-box c-size-lg c-theme-bg">
	<div class="container">
		<div class="row">
			<div class="c-shop-banner-3 c-center">
				<h3 class="c-title c-font-uppercase c-font-bold c-font-35 c-font-white">New Summer Collection</h3>
				<p class="c-desc c-font-uppercase c-font-bold c-font-25 c-font-white">70% Off With Promo</p>
				<button class="btn btn-lg c-btn-white c-btn-border-1x c-btn-square c-btn-bold">EXPLORE</button>
				<button class="btn btn-lg c-btn-white c-btn-square c-btn-bold">PURCHASE</button>
			</div>
			<div class="c-content-title-1 c-center c-margin-t-80">
				<h3 class="c-font-uppercase c-font-bold c-font-white">Life Should be Great Rather Than Long</h3>
				<p class="c-font-uppercase c-font-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam <br />nonummy et nibh euismod aliquam erat volutpat.</p>
				<a href="#" class="btn btn-lg c-btn-red c-btn-square c-font-uppercase">Purchase</a>
			</div>
		</div>
	</div>
</div>
<!-- END: CONTENT/MISC/PROMO-1-3 -->

<!-- BEGIN: CONTENT/stockS/stock-2-1 -->
<div class="c-content-box c-size-md c-overflow-hide c-bs-grid-small-space c-bg-grey-1">
	<div class="container">
		<div class="c-content-title-4">
			<h3 class="c-font-uppercase c-center c-font-bold c-line-strike"><span class="c-bg-grey-1">Most Popular</span></h3>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-label c-label-right c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/93.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Smartphone & Handset</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/95.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Canvas Shoes</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/91.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Shoes & Tie</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/88.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Smartphone & Handset</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-bg-red c-font-uppercase c-font-white c-font-13 c-font-bold">Sale</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/89.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Watches & Accessories</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/96.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Exclusive Watches</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/102.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Watches & Accessories</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 c-margin-b-20">
				<div class="c-content-product-2 c-bg-white">
					<div class="c-content-overlay">
						<div class="c-label c-theme-bg c-font-uppercase c-font-white c-font-13 c-font-bold">New</div>
						<div class="c-overlay-wrapper">
							<div class="c-overlay-content">
								<a href="shop-product-details.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
							</div>
						</div>
						<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 270px; background-image: url(assets/base/img/content/stock/100.jpg);"></div>
					</div>
					<div class="c-info">
						<p class="c-title c-font-18 c-font-slim">Titanium Watch</p>
						<p class="c-price c-font-16 c-font-slim">$548 &nbsp;
							<span class="c-font-16 c-font-line-through c-font-red">$600</span>
						</p>
					</div>
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group c-border-top" role="group">
							<a href="shop-product-wishlist.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Wishlist</a>
						</div>
						<div class="btn-group c-border-left c-border-top" role="group">
							<a href="shop-cart.html" class="btn btn-lg c-btn-white c-btn-uppercase c-btn-square c-font-grey-3 c-font-white-hover c-bg-red-2-hover c-btn-product">Cart</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/stockS/stock-2-1 -->

<!-- BEGIN: CONTENT/stockS/stock-3-1 -->
<div class="c-content-box c-overflow-hide c-bg-grey">
	<div class="c-content-product-3 c-bs-grid-reset-space">
		<div class="row">
			<div class="col-md-8 col-sm-6">
				<div class="c-content-overlay">
					<div class="c-overlay-wrapper">
						<div class="c-overlay-content">
							<a href="shop-product-details-2.html" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
						</div>
					</div>
					<div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 550px; background-image: url(assets/base/img/content/stock/21.jpg);"></div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="c-wrapper c-bg-red" style="height: 550px;">
					<div class="c-content c-border c-border-padding c-border-padding-right">
						<h3 class="c-title c-font-25 c-font-white c-font-uppercase c-font-bold">MOTO 360 WATCH</h3>
						<p class="c-description c-font-17 c-font-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy et nibh euismod aliquam erat volutpat.</p>
						<p class="c-description c-font-17 c-font-white">Dolor sit amet, consectetuer adipiscing elit sed diam nonummy</p>
						<p class="c-price c-font-55 c-font-white c-font-thin">$249</p>
						<a href="shop-product-details-2.html" class="btn btn-lg c-btn-white c-font-uppercase c-btn-square c-btn-border-1x">BUY NOW</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- END: CONTENT/stockS/stock-3-1 -->
		<!-- END: PAGE CONTENT -->


@endsection


@section('page_script_plugin')


<!-- BEGIN: PAGE SCRIPTS -->
<script src="assets/default/js/scripts/pages/extended-portfolio.js" type="text/javascript"></script>
<script src="assets/frontend/js/custom.js" type="text/javascript"></script>

<!-- END: PAGE SCRIPTS -->
@endsection
