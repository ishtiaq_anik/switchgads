@extends('admin.master')

@section('page_style_plugin')

<link href="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1> Photo {{ ($photo->id != "") ? __('edit'): __('create') }}
                    <small></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('photo_index') }}">Photos</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">{{ ($photo->id != "") ? __('edit') : __('create') }}</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->

        <div class="page-content-col">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <h3 class="c-center c-font-uppercase c-font-bold c-justify"><i class="icon-map"></i> {{$title}} </h3>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-multiple ajaxForm fileupload" id="ajaxForm" enctype='multipart/form-data'>
                                {{ csrf_field() }}
                                {{ ajax_action($route) }}
                                {{ ajax_method($method) }}
                                <?php if($photo->id){ ?>
                                    {{ ajax_id($photo->id) }}
                                <?php } ?>

                                <div class="row">
                                    <div class="col-lg-12">

                                        <!-- Category -->
                                        <div class="form-group row has-error-category">
                                            <label class="text-right col-md-4 control-label">Category<span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <select class="form-control select_dropdown" name="category" id="category"  data-target="#subcategory" data-action="{{ route('categories_subcategories_load') }}">
                                                    <option value="0">Select</option>
                                                    @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" <?php if($photo->category_id == $category->id){echo "selected";}?> >
                                                        {{ $category->title}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!--User Subdistrict-->
                                        <div class="form-group row has-error-subcategory">
                                            <label class="text-right col-md-4 control-label">Subcategory<span class="required" aria-required="true"> * </span></label>
                                            <div class="col-md-6">
                                                <select class="form-control" id="subcategory" name="subcategory" required="">
                                                    <option value="">-- Select --</option>
                                                    @if(isset($subcategory))
                                                    @foreach ($subcategories as $subcategory)
                                                    <option value="{{$subcategory->id}}" {{( isset($photo->subcategory) and $subcategory->id == $photo->subcategory->id)?" selected=''" : ""}}>{{$subcategory->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Title -->
                                        <div class="form-group row has-error-title">
                                            <label class="text-right col-md-4 control-label">Title<span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control form-control-sm" placeholder="title"
                                                name="title" value="<?php if($photo->title){echo $photo->title;}else{echo old('title');} ?>">
                                            </div>
                                        </div>

                                        <!-- Code Bangla -->
                                        <div class="form-group row has-error-slug">
                                            <label class="text-right col-md-4 control-label">Slug <span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control form-control-sm" placeholder="slug"
                                                name="slug" value="<?php if($photo->slug){echo $photo->slug;}else{echo old('slug');} ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row has-error-upload_file">
                                            <label class="text-right col-md-4 control-label">Upload <span class="c-font-red"> *</span> </label>
                                            <div class="col-md-6">
                                                @if($photo->uploaded_file)
                                                <div class="">
                                                    <img src="{{$project_url}}/storage/uploads/thumbnail/{{$photo->uploaded_file}}" alt="upload_file">
                                                </div>
                                                @endif
                                                <div class="fileinput <?php if($photo->uploaded_file){echo 'fileinput-exists';}else{echo 'fileinput-new';}?>" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                            <span class="fileinput-filename"> {{$photo->uploaded_file}}</span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new">Select </span>
                                                            <span class="fileinput-exists">Change </span>
                                                            <input type="file" name="upload_file">
                                                        </span>
                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
                                                    </div>
                                                    <p class="help-block">Successfully Updated</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group c-margin-t-40">
                                            <div class="col-sm-offset-5 col-md-7">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END PAGE BASE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
</div>
@endsection

@section('page_script_plugin')
<script src="{{asset('/assets/plugins/fancybox/jquery.fancybox.pack.js')}}" type="text/javascript"></script>

<script src="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
@endsection
@section('page_dependent_script_plugin')

<script src="{{asset('/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.bn.js')}}" charset="UTF-8"></script>
<script src="{{asset('/js/custom.js')}}" type="text/javascript"></script>


@endsection
