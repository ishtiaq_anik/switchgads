$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });

// DEMO CALL: callAjax({url:'/',method:'GET'}, 'successFunction:failedFunction:alwaysFunction');
function callAjax(params = {}, callback_functions = "") {

    let functions = callback_functions.split(":");
    let success_function = functions[0];
    let error_function = functions[1];
    let always_function = functions[2];

    let url = params.url;
    let method = (params.method) ? params.method : "GET";
    let optional = (params.optional) ? params.optional : "";
    delete params.url;
    delete params.method;
    delete params.optional;

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = parseInt(evt.loaded / evt.total);
                    NProgress.set(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: url,
        type: method,
        data: params
    }).done(function (response, status) {
        console.log(response);
        if (success_function !== undefined && success_function !== "") {
            console.log(success_function);
            if (optional != "") {
                response.optional = optional;
            }
            window[success_function](response, status);
        }
    }).fail(function (response, status) {
        if (error_function !== undefined && error_function !== "") {
            if (optional != "") {
                response.optional = optional;
            }
            window[error_function](response, status);
        }
    });
}

// DEMO CALL: callAjax({url:'/',method:'GET'}, 'successFunction:failedFunction:alwaysFunction');
function callFormAjax(params = {}, callback_functions = "") {
    console.log(callback_functions);
    let functions = callback_functions.split(":");
    let success_function = functions[0];
    let error_function = functions[1];
    let always_function = functions[2];

    let url = params.url;
    let method = (params.method) ? params.method : "GET";
    let optional = (params.optional) ? params.optional : "";
    delete params.url;
    delete params.method;
    delete params.optional;

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = parseInt(evt.loaded / evt.total);
                    NProgress.set(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: url,
        type: method,
        data: params,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false
    }).done(function (response, status) {
        console.log(response);
        if (success_function !== undefined && success_function !== "") {
            console.log(success_function);
            if (optional != "") {
                response.optional = optional;
            }
            if (response.data !== undefined) {
                if (response.data.redirect_to !== undefined) {
                    window.location = response.data.redirect_to;
                }
            }
            window[success_function](response, status);
        }
    }).fail(function (response, status) {
        if (error_function !== undefined && error_function !== "") {
            if (optional != "") {
                response.optional = optional;
            }
            window[error_function](response, status);
        }
    }).always(function (response) {
        if (always_function !== undefined && always_function !== "") {
            if (optional != "") {
                response.optional = optional;
            }
            window[always_function](response, status);
        }
    });
}

$.blockUI.defaults.css.border = 'none';
$.blockUI.defaults.css.padding = '15px';
$.blockUI.defaults.css.backgroundColor = '#000';
$.blockUI.defaults.css.borderRadius = '10px';
$.blockUI.defaults.css.opacity = .5;
$.blockUI.defaults.css.color = '#fff';

$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

function appAlert(message, status) {
    var container = ".c-content-panel .c-body";
    var place = "prepend";
    status = (status !== 'undefined') ? status : 'danger';
    // alert(message);
    App.alert({
        type: status,
        icon: status,
        message: message,
        container: container,
        place: place
    });
}
function validationCheck(data) {
    if (data.validation) {
        $(".form-group").removeClass("has-error").children("div").children(".help-block").remove();
        var errors = data.errors;
        for (var field in errors) {
            var group = $(".has-error-" + field);
            if (group.length) {
                if (!group.hasClass("has-error")) {
                    group.addClass("has-error");
                }
                group.children("div").append("<span class='help-block'><strong>" + errors[field][0] + "</strong></span>");
            }
        }
    }
}
/*
* Duplicate code
* for those pages where apply.js is not added
*/


function successAjaxForm(response) {
    if (response.success) {
        $(".has-error").children(".help-block").remove();

        if (response.data !== undefined) {
            if (response.data.redirect_to !== undefined) {
                window.location = response.data.redirect_to;
            }else{
                if (response.message !== undefined && response.message !== "") {
                    appAlert(response.message, 'success');
                }
            }
        }
    } else if (response.validation) {
        validationCheck(response);
    } else {
        if (response.message !== undefined && response.message !== "") {
            appAlert(response.message, 'danger');
        }
    }
}
function failedAjaxForm(response) {
    if (response.message !== undefined && response.message !== "") {
        appAlert(response.message, 'danger');
    }
}
$("#ajaxForm, .ajaxForm").on("submit", function (e) {
    e.preventDefault();

    let form_data = new FormData(this);
    let form = $(this);
    form_data.url = form.children('input[name="_action"]').val();
    form_data.method = form.children('input[name="_method"]').val();
    let success = form.children('input[name="_success"]').val();
    let failed = form.children('input[name="_failed"]').val();

    success = (typeof(success) !== 'undefined' && success !== "") ? success : "successAjaxForm";
    failed = (typeof(failed) !== 'undefined' && failed !== "") ? failed : "failedAjaxForm";
    NProgress.start();
    callFormAjax(form_data, success + ':' + failed);
});




$(".select_dropdown_checklist").on("change", function (e) {
    e.preventDefault();
    var current = $(this);
    var target = $(this).data('target');
    $.ajax({
        url : current.data('action'),
        method : 'post',
        data : {
            'id' : current.val()
        }
    }).done(function (response) {
        //    var option_text = $(target + " option:first-child").html();

        html = '';
        $.each(response.data, function( index, value ) {
            //    console.log(value.title);
            //    html = "I am Checklist";
            html += '<label class="mt-checkbox">'
            html +=    '<input type="checkbox" name="checkbox_a[]" id="inlineCheckbox'+value.id+'" value="'+value.id+'">'+value.title
            html +=    '<span></span>'
            html += '</label>';
        });
        $(target).html(html);
    }).fail(function (response) {
        appAlert(response.message, "danger", ".app-alert");
    });
});




$('#change_status').on('click', '.tool-action', function () {
    console.log('hello');
    var update_url = $("#change_status").children("input[name=status_update_url]").val();
    var ref_id = $("#change_status").children("input[name=ref_id]").val();
    window.change_status = $(this).data("value");
    //    window.change_status_message_requirement = $(this).data("message-required");
    $.ajax({
        url: update_url,
        method: "POST",
        data: {
            id: ref_id,
            status: window.change_status,
        }
    }).done(function (response) {
        if (response.success) {
            if (response.data.current) {
                var html = "<i class='" + response.data.current.icon_class + "'></i><span class='hidden-xs'> " + response.data.current.title + " </span>";
                html += Object.keys(response.data.next).length ? " <i class='fa fa-angle-down'></i>" : "";
                $("#change_status > a").removeClass().addClass("btn " + response.data.current.color).html(html);
            }
            if (response.data.next) {
                var options_html = "";
                $.each(response.data.next, function (index, option) {
                    options_html += "<li><a href='javascript:;' class='tool-action' data-value='" + index + "><i class='" + option.icon_class + "'></i> " + option.title + "</a></li>";
                });
                $("#change_status > ul").html(options_html);
            }
            if (response.data.redirect_to) {
                location.href = response.data.redirect_to;
            }
            appAlert(response.message, "success", ".app-alert");
        } else {
            appAlert(response.message, "danger", ".app-alert");
        }
    }).fail(function (response) {
        $("div.alert.alert-danger").removeClass('display-hide');
        appAlert(response.message, "danger", ".app-alert");
    });
    $('#confirm-status-modal').modal('show');
});


$(".select_dropdown").on("change", function (e) {
    console.log("Done");
    e.preventDefault();
    var current = $(this);
    var target = $(this).data('target');
    $.ajax({
        url : current.data('action'),
        method : 'post',
        data : {
            'id' : current.val()
        }
    }).done(function (response) {
        var option_text = $(target + " option:first-child").html();
        html = '<option value="">' + option_text + '</option>';
        $.each(response.data, function( index, value ) {
            html += '<option value="' +value.id+ '">' +value.title+ '</option>'
        });
        $(target).html(html);
    }).fail(function (response) {
        appAlert(response.message, "danger", ".app-alert");
    });
});

$(".select_dropdown_double").on("change", function (e) {
    e.preventDefault();

    let self = $(this);
    let params = {
        url: self.data('action-1'),
        method: 'POST',
        id: self.val(),
        optional: {
            target: self.data('target-1')
        }
    };
    callAjax(params, 'successChangeSelectOption:failedChangeSelectOption');
});


function successChangeSelectOption(response) {
    let target = response.optional.target;
    console.log(response.data);
    var option_text = $(target + " option:first-child").html();
    html = '<option value="0">' + option_text + '</option>';
    $.each(response.data, function (index, value) {
        html += '<option value="' + value.id + '">' + value.name + '</option>';
    });
    $(target).html(html);
}
function failedChangeSelectOption(response) {
    if (response.message !== undefined && response.message !== "") {
        appAlert(response.message, 'danger');
    }
}

$("#confirm-status-modal-confirm-btn").on('click', function () {
    $(".btn-note").click();
});



$("#price").on("change paste keyup", function (e){
    //    e.preventDefault();
    let current = $(this).val();
    let base_price = parseInt(current)+5;
    let base = '#base_price';
    $(base).html(base_price);
    //console.log(current);

});
$('#goedit').on('click',function(){
    $('#noedit').addClass('hide');
    //    $('#yesedit').removeClass('hide');
    $('#yesedit').html('<input class="" type="file" name="icon" style="padding-top:5px;" >');
});


if($("#product_category").val() > 0){
    $(document).ready(function(){
        //    e.preventDefault();


        var current = $("#product_category");
        var target = $("#product_category").data('target');

        $.ajax({
            url : current.data('action'),
            method : 'post',
            data : {
                'id' : current.val()
            }
        }).done(function (response) {
            var option_text = $(target + " option:first-child").html();
            html = '<option value="">' + option_text + '</option>';
            $.each(response.data, function( index, value ) {
                html += '<option value="' +value.id+ '">' +value.title+ '</option>'
            });
            $(target).html(html);
        }).fail(function (response) {
            appAlert(response.message, "danger", ".app-alert");
        });

        loadAccessories();
    });

    function loadAccessories(){
    //    console.log("hello");
        var current = $("#product_category");
        var target = $("#product_category").data('target-1');
        $.ajax({
            url : current.data('action-1'),
            method : 'post',
            data : {
                'id' : current.val()
            }
        }).done(function (response) {
            var option_text = $(target + " option:first-child").html();
            html =  '';
            $.each(response.data, function( index, value ) {

                html +=   '<div class="c-checkbox has-success">';
                html += '<input name="accessories[]" type="checkbox" value="'+value.id+'" id="checkbox-'+value.id+'" class="c-check">';
                html += '<label for="checkbox-'+value.id+'">';
                html += '<span class="inc"></span>';
                html += '<span class="check"></span>';
                html += '<span class="box"></span>'+value.title;
                html += '</label>';
                html += '</div>';
            });
            $(target).html(html);
        }).fail(function (response) {
            appAlert(response.message, "danger", ".app-alert");
        });
    }


}



$(document).ready(function () {

    NProgress.done();
});
