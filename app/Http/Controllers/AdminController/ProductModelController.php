<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use Illuminate\Http\Request;
use App\Models\Product\ProductModel;
use App\Http\Controllers\Controller;
use App\Models\Product\ProductCategory;
use App\Http\Datatables\ProductModelDatatable;

class ProductModelController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('product_model_index'),
            'dataload_url' => route('product_model_dataload'),
            'title' => "Product Model",
            'titles' => "Product Models",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "Category", "filter" => $this->filterText("category")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
        //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('product-model');
    }

    public function datatable(Request $request) {
        $ajax_table = new ProductModelDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $productcategories = ProductCategory::all();
        $productmodel = new ProductModel;
        $title = 'Product Model';
        $params = [
            'productmodel' => $productmodel,
            'productcategories' => $productcategories,
            'route'    => route('product_model_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.product.product-model-create', $params)->withNav('product-model');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:255',
        ];

        $validator = Validator::make($request->all(), $validator_data);

        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        // $category = Category::find($request->_ajax_id);
        if ($request->_ajax_id) {
            $productmodel = ProductModel::find($request->_ajax_id);
        } else {
            $productmodel = new ProductModel;
        }

        $productmodel->product_category_id = $request->input('product_category_id');
        $productmodel->title = $request->input('title');
        $productmodel->slug = $request->input('slug');

        if($productmodel->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('product_model_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];

        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\ProductModel  $productModel
    * @return \Illuminate\Http\Response
    */
    public function show(ProductModel $productModel)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\ProductModel  $productModel
    * @return \Illuminate\Http\Response
    */
    public function edit($productmodel_id)
    {
        $productmodel = ProductModel::find($productmodel_id);
        if($productmodel){
            $productcategories = ProductCategory::all();
            $title = 'Product Model';
            $params = [
                'productmodel' => $productmodel,
                'productcategories' => $productcategories,
                'route'    => route('product_model_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.product.product-model-create', $params);
        }
        return redirect()->route('product_model_index')->withNav('product-model');
    }


    public function productCategories(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $subcategories = MobileSubcategory::where('productmodel_id', $id)->get();
        if($subcategories) {
            $this->success();
            $this->message = "records found!";
            $this->data = $subcategories;
        }
        return $this->output();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\ProductModel  $productModel
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, ProductModel $productModel)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\ProductModel  $productModel
    * @return \Illuminate\Http\Response
    */
    public function destroy(ProductModel $productModel)
    {
        //
    }
}
