<?php

namespace App\Http\Components;
use View;
use App\Category;

trait Component {
    public $validation = 1;
    public $success = false;
    public $message = "Request processing failed";
    public $data = [];
    public $errors = [];
    public $html = [];

    public function validationError() {
        $data = [
            'validation' => $this->validation,
            'errors' => $this->errors,
            'message' => $this->message,
        ];
        return $data;
    }

    public function output() {
        $data = [
            'success' => $this->success,
            'data' => $this->data,
        ];

        if (count($this->message)) {
            $data['message'] = $this->message;
        }
        if (count($this->html)) {
            $data['html'] = $this->html;
        }

        return $data;
    }

    public function success() {
        $this->success = true;
    }

    public function setAlert($message) {
        session(['alert' => $message]);
    }

    public function setAlertPermission() {
        session(['alert' => "Currently You don't have permission to visit that page"]);
    }

    public function getAlert() {
        return (session()->has('alert') ? session()->pull('alert') : null);
    }

    public function setAlertCSSClass($type = "") {
        session(['alertType' => (($type == "success") ? "alert-success" : "alert-danger")]);
    }

    public function getAlertCSSClass() {
        return (session()->has('alertType') ? session()->pull('alertType') : "alert-danger");
    }

    public function getProjectUrl(){
        return rtrim(getenv('APP_URL'),'/');
    }

    public function getAppLocale(){
        return config('app.locale');
    }
    public function langTest(){
        $str = \Lang::get('dashboard.dashboard_title.dashboard');
        die($str);
    }

    public function storage_link($project_path, $file_path, $file)
    {
        $file_url = rtrim($project_path, '/') . '/storage/' . rtrim($file_path, '/') . '/' . ltrim($file, '/');
        return $file_url;
    }

    public function getCategory(){
        $categories = Category::all();
        return $categories;
    }

    public function getProductCategoryThumb($id){

        switch ($id) {
            case 1:
            $productThumbnail = 'assets/img/content/product-categories/iphone.png';
            break;

            case 2:
            $productThumbnail = 'assets/img/content/product-categories/smartphone.png';
            break;

            case 3:
            $productThumbnail = 'assets/img/content/product-categories/macbook.png';
            break;

            case 4:
            $productThumbnail = 'assets/img/content/product-categories/tablet.png';
            break;

            default:
            $productThumbnail = 'assets/img/content/product-categories/watch.png';
            break;

        }

        return $productThumbnail;
    }

    public function getProductStatus($status) {
        $status = [
            'current' => $this->productStatuses()[$status],
            'next' => $this->getProductNextStatuses($status),
        ];
        return $status;
    }

    public function getProductNextStatuses($status) {
        $next_statuses = [];
        if($status == "verification-on-going") {
            $next_statuses['verification-done-approved'] = $this->productStatuses()['verification-done-approved'];
            $next_statuses['verification-done-not-approved'] = $this->productStatuses()['verification-done-not-approved'];
        }

        return $next_statuses;
    }

    public function productStatuses()
    {
        return [
            'verification-on-going' => [
                'icon_class'        => 'icon-graph',
                'color'             => 'bg-yellow-saffron bg-font-yellow-saffron',
                'title'             => "Verification on going",
                'specific_title'    => "verification-on-going",
                'label'             =>"label-warning",
            ],

            'verification-done-approved' => [
                'icon_class'        => 'icon-user-follow',
                'color'             => 'bg-blue bg-font-blue',
                'title'             => "Approved",
                'specific_title'    => "verification-done-approved",
                'label'             =>"label-success",
            ],
            'verification-done-not-approved' => [
                'icon_class'        => 'icon-graph',
                'color'             => 'bg-red-intense bg-font-red-intense',
                'title'             => "Not Approved",
                'specific_title'    => "verification-done-not-approved",
                'label'             =>"label-danger",
            ],

            'permission-not-granted' => [
                'icon_class'        => 'icon-check',
                'color'             => 'bg-red-intense bg-font-red-intense',
                'title'             => "Permission not granted",
                'specific_title'    => "",
                'label'             =>"label-primary",
            ],
        ];
    }

    public function getProductStatusesForDatatableArray($status){
        $options = $this->productStatuses();
        foreach($options as $value){
            if($status == $value['specific_title']){
                $status_string ="<span class='label c-font-slim ".$value['label']."'>".ucwords($value['title'])."</span>" ;
            }
        }
        return $status_string;
    }

    

}
