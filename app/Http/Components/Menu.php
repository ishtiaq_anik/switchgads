<?php

namespace App\Http\Components;

use Auth;
use App\Http\Controllers\Controller;

class Menu extends Controller {


    public function adminDefaultMenu() {
        return [
            'start' => [
                ['visible' => true, 'tag' => 'dashboard', 'icon' => '<i class="icon-home"></i>', 'title' => "Dashboard", 'url' => 'dashboard',],
            ],
            'end' => [
                ['visible' => true, 'tag' => 'logout', 'icon' => '<i class="icon-logout"></i>', 'title' => "Logout", 'url' => 'logout',],
            ],
        ];
    }

    public function adminMenu() {
        return [



            'items' => [
                ['visible' => true, 'tag' => 'dashboard', 'icon' => '<i class="icon-home"></i>', 'title' => "Dashboard", 'url' => '/dashboard'],

                ['visible' => true, 'tag' => 'product-type', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Product Category", 'url' => '/product-type'],

                ['visible' => false, 'tag' => 'product-specs', 'icon' => '<i class="icon-home"></i>', 'title' => "Product Specs", 'url' => 'javascript;',],

                ['visible' => true, 'tag' => 'product-model', 'icon' => '<i class="icon-picture"></i>', 'title' => "Product Model", 'url' => '/product-model'],

                ['visible' => true, 'tag' => 'product-accessory', 'icon' => '<i class="icon-picture"></i>', 'title' => "Product Accessory", 'url' => '/product-accessory'],

                [
                    'visible' => true, 'tag' => 'mobile', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Mobile", 'url' => '/mobile',
                    'subitems'=> [
                        ['visible' => true, 'tag' => 'mobile-condition', 'icon' => '<i class="icon-picture"></i>', 'title' => "Condition", 'url' => '/mobile-condition'],
                        ['visible' => true, 'tag' => 'mobile-storage', 'icon' => '<i class="icon-picture"></i>', 'title' => "Storage", 'url' => '/mobile-storage'],
                        ['visible' => true, 'tag' => 'mobile-color', 'icon' => '<i class="icon-picture"></i>', 'title' => "Color", 'url' => '/mobile-color'],


                    ],
                ],
                [
                    'visible' => true, 'tag' => 'photos', 'icon' => '<i class="icon-picture"></i>', 'title' => "Photos", 'url' => '/photos',
                    'subitems'=> [
                        ['visible' => true, 'tag' => 'categories', 'icon' => '<i class="icon-picture"></i>', 'title' => "Category", 'url' => '/categories'],
                        ['visible' => true, 'tag' => 'subcategories', 'icon' => '<i class="icon-picture"></i>', 'title' => "Subcategory", 'url' => '/subcategories'],
                        ['visible' => true, 'tag' => 'upload', 'icon' => '<i class="icon-picture"></i>', 'title' => "Upload", 'url' => '/photos'],

                    ],
                ],
                [
                    'visible' => true, 'tag' => 'listed-products', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Listed Products", 'url' => '/products',
                    'subitems'=> [
                        ['visible' => true, 'tag' => 'iphone', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Iphone", 'url' => 'product-listing/iphone/index'],
                        ['visible' => true, 'tag' => 'smartphone', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Smartphone", 'url' => 'product-listing/smartphone/index'],
                    ],
                ],
                [
                    'visible' => true, 'tag' => 'users', 'icon' => '<i class="icon-user"></i>', 'title' => "Users", 'url' => '/users',
                    'subitems'=> [
                        ['visible' => true, 'tag' => 'seller', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Seller", 'url' => 'users/seller/index'],
                        ['visible' => true, 'tag' => 'buyer', 'icon' => '<i class="icon-screen-smartphone"></i>', 'title' => "Buyer", 'url' => 'users/buyer/index'],
                    ],
                ],
                ['visible' => false, 'tag' => 'product-listing', 'icon' => '<i class="icon-home"></i>', 'title' => "Product Listing", 'url' => 'javascript;',],


                // ['visible' => true, 'tag' => 'smartphones', 'icon' => '<i class="icon-picture"></i>', 'title' => "Smartphone", 'url' => '/smartphones'],
                // ['visible' => true, 'tag' => 'macbooks', 'icon' => '<i class="icon-picture"></i>', 'title' => "Macbook", 'url' => '/macbooks'],
            ],
        ];
    }

    public function generateMenu($items, $nav, $subnav) {
        $html = "<ul class='page-sidebar-menu' data-keep-expanded='false' data-auto-scroll='true' data-slide-speed='200'>";
        foreach ($items as $key => $item) {
            if ($item['visible']) {
                $class = $nav == $item['tag'] ? 'active open' : '';
                $url = isset($item['url']) ? url($item['url']) : "javascript:;";
                //    $has_subitem = isset($item['items']) ? true : false;
                $has_subitem = isset($item['subitems']) ? true : false;
                //    dd($has_subitem);
                $open = "";
                if ($has_subitem && $subnav != "") {
                    $open = $this->hasSubnav($item['subitems'], $subnav);
                }

                $html .= "<li class='nav-item " . $class . "'>";
                $html .= "<a href=$url class='nav-link nav-toggle'>";
                $html .= $item['icon'];
                $html .= "<span class='title'>" . $item['title'] . "</span>";
                $html .= $has_subitem ? "<span class='arrow " . $open . "'></span>" : "";
                $html .= "</a>";

                if ($has_subitem) {
                    $html .= "<ul class='sub-menu'>";
                    $subitems = $item['subitems'];
                    //dd($subitems);
                    foreach ($subitems as $subitem) {

                        if ($subitem['visible']) {
                            $class = $subnav == $subitem['tag'] ? 'active open' : '';
                            $url = url($subitem['url']);
                            $html .= "<li class='nav-item " . $class . "'>";
                            $html .= "<a href=$url class='nav-link'>";
                            $html .= "<span class='title'> " . $subitem['title'] . " </span>";
                            $html .= "</a>";
                            $html .= "</li>";
                        }
                    }
                    $html .= "</ul>";
                }
                $html .= "</li>";
            }
        }
        $html .= "</ul>";
        return $html;
    }

    public function widget($nav = "", $subnav = "") {
        $defaultData = $this->adminDefaultMenu();
        $data = $this->adminMenu();
        //        $items = array_merge($defaultData['start'], $data['items'], $defaultData['end']);
        $items = $data['items'];

        return $this->generateMenu($items, $nav, $subnav);
    }

    public function menuPermission($key = '') {
        $permissions = json_decode(Auth::user()->menus, true);
        if (isset($permissions) && is_array($permissions)) {
            return in_array($key, $permissions);
        }
        return false;
    }

    public function subMenuPermission($subkey = '', $key = '') {
        return true;
    }

    public function hasSubnav($items, $subnav) {

        foreach ($items as $subitems) {
            if ($subnav == $subitems['tag']) {
                return "open";
            }
        }
        return "";
    }

    public static function menuWidget($nav = "", $subnav = "") {
        $menu = new Menu;
        return $menu->widget($nav, $subnav);
    }

    public function getIcons($nav, $plural = false)
    {
        return '<i class="icon-home"></i>';

        die($this->adminMenu());
        foreach ($this->adminMenu() as $item) {
            if($item['tag'] == $nav) {
                if($plural && isset($item['icons'])) {
                    return $item['icons'];
                }else {
                    return $item['icon'];
                }
            }
        }
        return $this->default_icon;
    }

}
