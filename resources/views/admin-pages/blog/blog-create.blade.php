@extends('admin.master')

@section('page_style_plugin')

<link href="{{asset('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/assets/plugins/icheck/skins/all.css ')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1> Blog {{ ($blog->id != "") ? __('edit'): __('create') }}
                    <small></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('blog_index') }}">Blogs</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">{{ ($blog->id != "") ? __('edit') : __('create') }}</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->

        <div class="page-content-col">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <h3 class="c-center c-font-uppercase c-font-bold c-justify"><i class="icon-map"></i> {{$title}} </h3>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-multiple ajaxForm fileupload" id="ajaxForm" enctype='multipart/form-data'>
                                {{ csrf_field() }}
                                {{ ajax_action($route) }}
                                {{ ajax_method($method) }}
                                <?php if($blog->id){ ?>
                                    {{ ajax_id($blog->id) }}
                                <?php } ?>

                                <div class="row">
                                    <div class="col-lg-12">

                                        <!-- Category -->
                                        <div class="form-group row has-error-category">
                                            <label class="text-right col-md-4 control-label">Category<span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <select class="form-control select_dropdown_checklist" name="category" id="category"  data-target="#c-subcategory" data-action="{{ route('blog_categories_subcategories_load') }}">
                                                    <option value="0">Select</option>
                                                    @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" <?php if($blog->category_id == $category->id){echo "selected";}?> >
                                                        {{ $category->title}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row has-error-check">
                                            <label class="text-right col-md-4 control-label">Tag</label>
                                            <div class="col-md-6">
                                                <!-- <p>Select Category First</p> -->
                                                <div id="c-subcategory" name="c-subcategory" class="mt-checkbox-inline ">
                                                    <?php  // $blog_tag = json_decode($blogs->subcategory_id, true); ?>
                                                    @if(isset($subcategory))
                                                    @foreach ($subcategories as $subcategory)
                                                    <label class="mt-checkbox">
                                                        <input type="checkbox" id="inlineCheckbox1" value="option1"> {{$subcategory->title}}
                                                        <span></span>
                                                    </label>
                                                    @endforeach
                                                    @endif
                                                </div>

                                            </div>
                                        </div>

                                        <!-- Title -->
                                        <div class="form-group row has-error-title">
                                            <label class="text-right col-md-4 control-label">Title<span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control form-control-sm" placeholder="title"
                                                name="title" value="<?php if($blog->title){echo $blog->title;}else{echo old('title');} ?>">
                                            </div>
                                        </div>

                                        <!-- Code Bangla -->
                                        <div class="form-group row has-error-slug">
                                            <label class="text-right col-md-4 control-label">Slug <span class="c-font-red"> *</span>  </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control form-control-sm" placeholder="slug"
                                                name="slug" value="<?php if($blog->slug){echo $blog->slug;}else{echo old('slug');} ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row has-error-upload_file">
                                            <label class="text-right col-md-4 control-label">Upload <span class="c-font-red"> *</span> </label>
                                            <div class="col-md-6">
                                                @if($blog->photo_link)
                                                <div class="">
                                                    <img src="{{$project_url}}/storage/blogs/thumbnail/{{$blog->photo_link}}" alt="photo_file">
                                                </div>
                                                @endif
                                                <div class="fileinput <?php if($blog->uploaded_file){echo 'fileinput-exists';}else{echo 'fileinput-new';}?>" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                            <span class="fileinput-filename"> {{$blog->uploaded_file}}</span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new">Select </span>
                                                            <span class="fileinput-exists">Change </span>
                                                            <input type="file" name="upload_file">
                                                        </span>
                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
                                                    </div>
                                                    <p class="help-block">Successfully Updated</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group c-margin-t-40">
                                            <div class="col-sm-offset-5 col-md-7">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END PAGE BASE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
</div>
@endsection

@section('page_script_plugin')
<script src="{{asset('/assets/plugins/fancybox/jquery.fancybox.pack.js')}}" type="text/javascript"></script>

<script src="{{asset('/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/icheck/icheck.min.js')}}" type="text/javascript"></script>
@endsection
@section('page_dependent_script_plugin')

<script src="{{asset('/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{asset('/assets/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.bn.js')}}" charset="UTF-8"></script>
<script src="{{asset('/js/custom.js')}}" type="text/javascript"></script>


@endsection
