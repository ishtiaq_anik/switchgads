<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Blog;
use App\BlogCategory;
use App\BlogSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Datatables\BlogDatatable;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = [
            'base_url' => route('blog_index'),
            'dataload_url' => route('blog_dataload'),
            'title' => "Blog",
            'titles' => "Blogs",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "Category", "filter" => $this->filterText("category")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
       // dd($params);
        return view('admin.table', $params)->withNav('blogs')->withSubnav('post');
    }

    public function datatable(Request $request) {
        $ajax_table = new BlogDatatable;
        return $ajax_table->table($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blog = new Blog;
        $categories  = BlogCategory::all();
        $subcategories  = BlogSubcategory::all();
        $title = 'blog';
        $params = [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'blog' => $blog,
            'route'    => route('blog_store'),
            'method'   => 'post',
            'title'    => $title,
        ];

    //    dd($categories);
        return view('admin-pages.blog.blog-create', $params)->withNav('blogs')->withSubnav('post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // var_dump($request->all());
       // die();


        $validator_data = [
            'title'      => 'required|max:500',
            'slug'     => 'required|max:500',
            'upload_file'   => 'required|mimes:png,jpg,jpeg,pdf|max:5120',//5MB
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please fill all the required field. ";
            return $this->validationError();
        }
        $file = ($request->hasFile('upload_file')) ? $this->uploadFiles($request->file('upload_file'),'blogs') : "";
        $blog = new Blog;
        $blog->title= $request->input('title');
        $blog->slug = $request->input('slug');
        $blog->category_id = $request->input('category');
        $blog->subcategory_id = isset($request->checkbox_a) ? json_encode($request->checkbox_a) : "";
        $blog->description = $request->input('description');
        $blog->photo_link= $file;
        if($blog->save()){
            $this->success();
            $default = "";
            $this->message = "";
            $data['redirect_to'] = route('blog_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
