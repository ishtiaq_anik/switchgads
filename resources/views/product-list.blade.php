@extends('layouts.master')

@section('title', 'Product List')
@section('content')


<div class="container">

    <section>

        <div class="c-layout-sidebar-menu c-theme ">
            <!-- BEGIN: LAYOUT/SIDEBARS/SHOP-SIDEBAR-MENU-2 -->
            <div class="c-sidebar-menu-toggler">
                <h3 class="c-title c-font-uppercase c-font-bold">Navigation</h3>
                <a href="javascript:;" class="c-content-toggler" data-toggle="collapse" data-target="#sidebar-menu-1">
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                    <span class="c-line"></span>
                </a>
            </div>

            <!-- BEGIN: CONTENT/SHOPS/SHOP-FILTER-SEARCH-1 -->
            <ul class="c-shop-filter-search-1 list-unstyled">

                <li>
                    <label class="control-label c-font-uppercase c-font-bold">Condition</label>

                    <div class="c-checkbox c-checkbox-height">
                        <input type="checkbox" id="checkbox-sidebar-3-2" class="c-check"> <label for="checkbox-sidebar-3-2">
                            <span class="inc"></span> <span class="check"></span> <span class="box"></span>
                            <p>New (Retail)</p>
                        </label>
                    </div>
                    <div class="c-checkbox c-checkbox-height">
                        <input type="checkbox" id="checkbox-sidebar-3-2" class="c-check"> <label for="checkbox-sidebar-3-2">
                            <span class="inc"></span> <span class="check"></span> <span class="box"></span>
                            <p>New (Resale)</p>
                        </label>
                    </div>
                    <div class="c-checkbox c-checkbox-height">
                        <input type="checkbox" id="checkbox-sidebar-3-2" class="c-check"> <label for="checkbox-sidebar-3-2">
                            <span class="inc"></span> <span class="check"></span> <span class="box"></span>
                            <p>Mint</p>
                        </label>
                    </div>
                    <div class="c-checkbox c-checkbox-height">
                        <input type="checkbox" id="checkbox-sidebar-3-2" class="c-check"> <label for="checkbox-sidebar-3-2">
                            <span class="inc"></span> <span class="check"></span> <span class="box"></span>
                            <p>Good</p>
                        </label>
                    </div>
                    <div class="c-checkbox c-checkbox-height">
                        <input type="checkbox" id="checkbox-sidebar-3-2" class="c-check"> <label for="checkbox-sidebar-3-2">
                            <span class="inc"></span> <span class="check"></span> <span class="box"></span>
                            <p>Fair</p>
                        </label>
                    </div>
                </li>


                <li>
                    <label class="control-label c-font-uppercase c-font-bold">Color</label>
                    <div class="input-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="checkbox-sidebar-1-1" class="c-check"> <label for="checkbox-sidebar-1-1">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> Black
                            </label>
                        </div>
                        <div class="c-checkbox">
                            <input type="checkbox" id="checkbox-sidebar-1-2" class="c-check"> <label for="checkbox-sidebar-1-2">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> Blue
                            </label>
                        </div>
                        <div class="c-checkbox">
                            <input type="checkbox" id="checkbox-sidebar-1-3" class="c-check"> <label for="checkbox-sidebar-1-3">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> Gold
                            </label>
                        </div>
                        <div class="c-checkbox c-checkbox-height">
                            <input type="checkbox" id="checkbox-sidebar-1-4" class="c-check"> <label for="checkbox-sidebar-1-4">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> Silver
                            </label>
                        </div>
                        <div class="c-checkbox c-checkbox-height">
                            <input type="checkbox" id="checkbox-sidebar-1-4" class="c-check"> <label for="checkbox-sidebar-1-4">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> Red
                            </label>
                        </div>
                    </div>
                </li>
                <li>
                    <label class="control-label c-font-uppercase c-font-bold">Storage</label>
                    <div class="input-group">
                        <div class="c-checkbox">
                            <input type="checkbox" id="checkbox-sidebar-1-1" class="c-check"> <label for="checkbox-sidebar-1-1">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> 16 GB
                            </label>
                        </div>
                        <div class="c-checkbox">
                            <input type="checkbox" id="checkbox-sidebar-1-2" class="c-check"> <label for="checkbox-sidebar-1-2">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> 32 GB
                            </label>
                        </div>
                        <div class="c-checkbox">
                            <input type="checkbox" id="checkbox-sidebar-1-3" class="c-check"> <label for="checkbox-sidebar-1-3">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> 64 GB
                            </label>
                        </div>
                        <div class="c-checkbox c-checkbox-height">
                            <input type="checkbox" id="checkbox-sidebar-1-4" class="c-check"> <label for="checkbox-sidebar-1-4">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> 128 GB
                            </label>
                        </div>
                        <div class="c-checkbox c-checkbox-height">
                            <input type="checkbox" id="checkbox-sidebar-1-4" class="c-check"> <label for="checkbox-sidebar-1-4">
                                <span class="inc"></span> <span class="check"></span> <span class="box"></span> 256 GB
                            </label>
                        </div>
                    </div>
                </li>

                <li>
                    <label class="control-label c-font-uppercase c-font-bold">Price Range</label>
                    <div class="c-price-range-box input-group">
                        <div class="c-price input-group col-md-6 pull-left">
                            <span class="input-group-addon c-square c-theme">$</span>
                            <input type="text" class="form-control c-square c-theme" placeholder="from">
                        </div>
                        <div class="c-price input-group col-md-6 pull-left">
                            <span class="input-group-addon c-square c-theme">$</span>
                            <input type="text" class="form-control c-square c-theme" placeholder="to">
                        </div>
                    </div>
                </li>
            </ul><!-- END: CONTENT/SHOPS/SHOP-FILTER-SEARCH-1 -->

        </div>
    </section>
    <section>

        <div class="c-layout-sidebar-content ">
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->
            <div class="c-shop-result-filter-1 clearfix form-inline">
                <div class="c-filter">
                    <label class="control-label c-font-16">Show:</label>
                    <select class="form-control c-square c-theme c-input">
                        <option value="#?limit=24" selected="selected">24</option>
                        <option value="#?limit=25">25</option>
                        <option value="#?limit=50">50</option>
                        <option value="#?limit=75">75</option>
                        <option value="#?limit=100" selected="">100</option>
                    </select>
                </div>
                <div class="c-filter">
                    <label class="control-label c-font-16">Sort&nbsp;By:</label>
                    <select class="form-control c-square c-theme c-input">
                        <option value="#?sort=p.sort_order&amp;order=ASC" selected="selected">Default</option>
                        <option value="#?sort=pd.name&amp;order=ASC">Name (A - Z)</option>
                        <option value="#?sort=pd.name&amp;order=DESC">Name (Z - A)</option>
                        <option value="#?sort=p.price&amp;order=ASC">Price (Low &gt; High)</option>
                        <option value="#?sort=p.price&amp;order=DESC" selected="">Price (High &gt; Low)</option>
                        <option value="#?sort=rating&amp;order=DESC">Rating (Highest)</option>
                        <option value="#?sort=rating&amp;order=ASC">Rating (Lowest)</option>
                        <option value="#?sort=p.model&amp;order=ASC">Model (A - Z)</option>
                        <option value="#?sort=p.model&amp;order=DESC">Model (Z - A)</option>
                    </select>
                </div>
            </div><!-- END: CONTENT/SHOPS/SHOP-RESULT-FILTER-1 -->

            <div class="c-margin-t-20"></div>

            <!-- BEGIN: CONTENT/SHOPS/SHOP-2-8 -->
            @foreach($products as $product)
            <div class="row c-margin-b-40">
                <div class="c-content-product-2 c-bg-white">
                    <div class="col-md-3">
                        <div class="c-content-overlay">
                            <div class="c-overlay-wrapper">
                                <div class="c-overlay-content">
                                    <a href="{{ route('product_details',$product->id) }}" class="btn btn-md c-btn-grey-1 c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">Explore</a>
                                </div>
                            </div>
                            <div class="c-bg-img-center c-overlay-object" data-height="height" style="height: 180px; background-image: url({{asset('assets/img/content/shop5/18.png')}});"></div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="c-info-list">
                            <h3 class="c-title c-font-bold c-font-22 c-font-dark">
                                <a class="c-theme-link" href="{{ route('product_details',$product->id) }}">{{$product['title']}}</a>
                            </h3>
                            <p class="c-price c-font-20 c-font-thin">{{$product['price']}} &nbsp;</p>
                            <p class="c-price c-font-14 c-font-thin">{{$product->shipping_location}}, {{$product->shipping_country}} &nbsp;</p>
                            <p class="c-review-star">
                                <span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span>
                                <span class="fa fa-star c-theme-font"></span> <span class="fa fa-star c-theme-font"></span> (18)
                            </p>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-sm c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">
                                <i class="fa fa-shopping-cart"></i>Buy Now
                            </button>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="c-info-list">
                            <h3 class="c-title c-font-bold c-font-22 c-font-dark">
                                <a class="c-theme-link" href="shop-product-details.html">&nbsp</a>
                            </h3>
                            <p class="c-price c-font-16 c-font-thin">{{$product['tracking_id']}}  &nbsp;
                                <span class="c-font-16 c-font-red"></span>
                            </p>
                            <p class="c-price c-font-16 c-font-thin">{{ $product->color->title }} &nbsp;

                            </p>
                        </div>

                    </div>
                </div>
            </div>

            @endforeach



            <div class="c-margin-t-20"></div>

            <ul class="c-content-pagination c-square c-theme pull-right">
                <li class="c-prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li><a href="#">1</a></li>
                <li class="c-active"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li class="c-next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
            </ul>			<!-- END: PAGE CONTENT -->
        </div>
    </section>
</div>

@endsection
