<?php

namespace App\Http\Datatables;

use App\Models\Product\ProductModel;
use App\Components;
use Auth;
use Illuminate\Http\Request;

class ProductModelDatatable extends Datatable {

    private $nav = 'product-model';

    public function table(Request $request) {

        $this->deleteTableItem($request);
        $results = new ProductModel;
        $title = isset($request->title) ? $request->title : null;
        $slug = isset($request->slug) ? $request->slug : null;
        $category = isset($request->product_category_id) ? $request->product_category_id : null;
        $updated_from = isset($request->updated_from) ? $request->updated_from : null;
        $updated_to = isset($request->updated_to) ? $request->updated_to : null;

        if ($title) {
            $results = $results->where('title', 'like', '%' . $title . '%');
        }
        if ($slug) {
            $results = $results->where('slug', 'like', '%' . $slug . '%');
        }
        if ($category) {
            $results = $results->where('category', 'like', '%' . $slug . '%');
        }
        if ($updated_from) {
            $results = $results->where('updated_at', '>=', $this->date_filter($updated_from));
        }
        if ($updated_to) {
            $results = $results->where('updated_at', '<=', $this->date_filter($updated_to, true));
        }
        $tableColumns = [
            "",
            "title",
            "slug",
            "category",
            "updated_at",
            ""
        ];
        $sortColumn = $request->order[0]['column'];
        $sortDir = $request->order[0]['dir'];
        $sort_field = $tableColumns[$sortColumn];

        switch ($sort_field) {
            case "title":
            $results = $results->orderBy('type', $sortDir);
            break;
            case "slug":
            $results = $results->orderBy('type', $sortDir);
            break;
            case "category":
            $results = $results->orderBy('type', $sortDir);
            break;
            case "updated_at":
            $results = $results->orderBy('updated_at', $sortDir);
            break;
        }

        $results = $results->get();

        $iTotalRecords = $results->count();
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $data = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $result = $results[$i];

            $edit = "<a class='btn btn-sm btn-outline blue table-row-edit' title='Edit' href='".route('product_model_edit', $result->id)."' data-id='" . $result->id . "'> <i class='icon-note'></i>Edit</a>";
            $delete = "<a class='btn btn-sm btn-outline red table-row-delete' title='Remove' href='javascript:;' data-id='" . $result->id . "'> <i class='icon-trash'></i>Delete</a>";

            $data[] = [
                $i+1,
                isset($result->title) ? $result->title : "",
                isset($result->slug) ? $result->slug : "",
                isset($result->category) ? $result->category->title : "",
                isset($result->updated_at) ? date_format($result->updated_at, 'd/m/Y') : "",
                (isset($edit) ? $edit : "") . (isset($delete) ? $delete : ""),
            ];
        }
        $this->data = $data;
        $this->draw = $sEcho;
        $this->total = $iTotalRecords;
        $this->filtered = $iTotalRecords;
        return $this->outputDatatable();
    }

    public function deleteTableItem($request) {
        if (isset($request->actionType) && $request->actionType == "delete_action") {
            $productmodel = ProductModel::find($request->record_id);
            if ($productmodel) {
                $productmodel->delete();
                $this->status = "OK";
                $this->message = "Mobile Category deleted successfully";
            } else {
                $this->message = "Mobile Category delete failed";
            }
        }
    }

}
