<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Request;

class MobileCategoryTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $categories = [];
        for ($i = 0; $i < 10; $i++) {
            $categories[] = [
                'title' => 'Category' . $i
            ];

        }
        \App\Category::insert($categories);
    }
}
