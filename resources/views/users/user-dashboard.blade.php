@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Profile Dashboard</h3>
                <h4 class=""></h4>
            </div>

        </div>
    </div>

</section>
<div class="container">
    <section>

        @include('layouts.side-menu')

    </section>
    <section>
        <div class="c-layout-sidebar-content ">
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="c-content-title-1">
                        <h3 class="c-font-uppercase c-font-bold">My Dashboard</h3>
                        <div class="c-line-left"></div>
                        <p class="">
                            Hello <a href="#" class="c-theme-link">{{$sellername}}</a> (not <a href="#" class="c-theme-link">{{$sellername}}</a>? <a href="#" class="c-theme-link">Sign out</a>). <br>
                        </p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Info<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <h3 class="c-font-uppercase c-font-bold">{{ $sellername }}</h3>
                            <ul class="list-unstyled">
                                <li>25, Lorem Lis Street, Orange C, California, US</li>
                                <li>Phone: 800 123 3456</li>
                                <li>Fax: 800 123 3456</li>
                                <li>Email: <a href="{{$selleremail}}" class="c-theme-link">{{$selleremail}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 c-margin-b-20">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Info<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <h3 class="c-font-uppercase c-font-bold">{{ $sellername }}</h3>
                            <ul class="list-unstyled">
                                <li>25, Lorem Lis Street, Orange C, California, US</li>
                                <li>Phone: 800 123 3456</li>
                                <li>Fax: 800 123 3456</li>
                                <li>Email: <a href="mailto:switchgads@themehats.com" class="c-theme-link">switchgads@themehats.com</a></li>
                            </ul>
                        </div>
                    </div>


                </div>

            </div><!-- END: CONTENT/SHOPS/SHOP-CUSTOMER-DASHBOARD-1 -->
            <!-- END: PAGE CONTENT -->
        </div>
    </section>

</div>
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->


@endsection
