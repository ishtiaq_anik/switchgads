<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\MobileCondition;
use App\Http\Controllers\Controller;
use App\Http\Datatables\MobileConditionDatatable;
use Illuminate\Http\Request;

class MobileConditionController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('mobile_condition_index'),
            'dataload_url' => route('mobile_condition_dataload'),
            'title' => "Mobile Condition",
            'titles' => "Mobile Conditions",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];
        //     dd($params);

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('mobile')->withSubnav('mobile-condition');
    }

    public function datatable(Request $request) {
        $ajax_table = new MobileConditionDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $mobilecondition = new MobileCondition;
        $title = 'Mobile Condition';
        $params = [
            'mobilecondition' => $mobilecondition,
            'route'    => route('mobile_condition_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.mobile.mobile-condition-create', $params)->withNav('mobile')->withSubnav('mobile-condition');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
         // var_dump($request->all());
         // die();

        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:255|unique:mobile_conditions,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        if ($request->_ajax_id) {

            $mobilecondition = MobileCondition::find($request->_ajax_id);
        } else {
            $mobilecondition = new MobileCondition;
        }

        $mobilecondition->title = $request->input('title');
        $mobilecondition->slug = $request->input('slug');
        if($mobilecondition->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('mobile_condition_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\MobileCondition  $mobileCondition
    * @return \Illuminate\Http\Response
    */
    public function show(MobileCondition $mobileCondition)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\MobileCondition  $mobileCondition
    * @return \Illuminate\Http\Response
    */
    public function edit($mobilecondition_id)
    {
        $mobilecondition = MobileCondition::find($mobilecondition_id);
        if($mobilecondition){
            $title = 'Mobile Condition';
            $params = [
                'mobilecondition' => $mobilecondition,
                'route'    => route('mobile_condition_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.mobile.mobile-condition-create', $params);
        }
        return redirect()->route('mobile_condition_index')->withNav('mobiles');
    }


    public function subconditions(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $subconditions = MobileSubcondition::where('mobilecondition_id', $id)->get();
        if($subconditions) {
            $this->success();
            $this->message = "records found!";
            $this->data = $subconditions;
        }
        return $this->output();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\MobileCondition  $mobileCondition
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, MobileCondition $mobileCondition)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\MobileCondition  $mobileCondition
    * @return \Illuminate\Http\Response
    */
    public function destroy(MobileCondition $mobileCondition)
    {
        //
    }
}
