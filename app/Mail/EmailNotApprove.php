<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Product;
// use File;
// use Illuminate\Support\Facades\Storage;

class EmailNotApprove extends Mailable
{
    use Queueable, SerializesModels;

    public $id;
    public $product;
    public $applicant_name;
    public $tracking_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->product = Product::find($this->id);
        $this->applicant_name = $this->product->seller['name'];
        $this->tracking_id = $this->product->tracking_id;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Switchgads Team')->markdown('emails.product.product-not-approve');
    }
}
