<meta charset="utf-8" />
<title>Metronic Admin Theme #4 | Admin Dashboard 2</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta content="Preview page of Metronic Admin Theme #4 for statistics, charts, recent events and reports" name="description" />
<meta content="" name="author" />
