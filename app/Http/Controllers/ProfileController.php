<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Product\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller {



    public function userProfileDashboard() {

           $sellername = Auth::user()->name;
           $selleremail = Auth::user()->email;

        $params = [
            'productTypes' => $this->productTypes,
            'sellername' =>$sellername,
            'selleremail' =>$selleremail,
            'sidenav' =>'dashboard',
        ];
        return view('users.user-dashboard', $params)->withNav('user-dashboard');
    }
    public function userProfileEdit() {

        $params = [
            'route' => route('user_profile_store'),
            'productTypes' => $this->productTypes,
            'sidenav' =>'profile',
        ];
        return view('users.user-profile-edit', $params)->withNav('user-profile');
    }

    public function userSellingHistory() {

        $sellerid = Auth::user()->id;

        $products = Product::where('user_id', $sellerid)->get();
    //    dd($product->count());
        $params = [
            'products' => $products,
            'productTypes' => $this->productTypes,
            'sidenav' =>'history',
        ];
        return view('users.user-selling-history', $params)->withNav('home');
    }




}
