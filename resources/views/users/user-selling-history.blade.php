@extends('layouts.master')

@section('title', 'Seller History')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Profile Dashboard</h3>
                <h4 class=""></h4>
            </div>

        </div>
    </div>

</section>

<div class="container">


    <section>

        @include('layouts.side-menu')

    </section>


    <section>

        <div class="c-layout-sidebar-content ">
            <!-- BEGIN: PAGE CONTENT -->
            <!-- BEGIN: CONTENT/SHOPS/SHOP-ORDER-HISTORY-2 -->
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-font-bold">Listing History</h3>
            </div>
            <div class="row c-margin-b-40 c-order-history-2">
                <div class="row c-cart-table-title">
                    <div class="col-md-2 c-cart-image">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Image</h3>
                    </div>
                    <div class="col-md-2 c-cart-ref">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Tracking Id</h3>
                    </div>
                    <div class="col-md-2 c-cart-ref">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Category</h3>
                    </div>
                    <div class="col-md-2 c-cart-desc">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Title</h3>
                    </div>
                    <div class="col-md-2 c-cart-price">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Price</h3>
                    </div>
                    <div class="col-md-2 c-cart-qty">
                        <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Action</h3>
                    </div>
                </div>

                @foreach($products as $product)
                <!-- BEGIN: ORDER HISTORY ITEM ROW -->
                <div class="row c-cart-table-row">
                    <h2 class="c-font-uppercase c-font-bold c-theme-bg c-font-white c-cart-item-title c-cart-item-first">Item 1</h2>
                    <div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
                        <img src="{{asset('storage/thumb/thumbnail/'.$product->product_thumb)}}">
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-6 c-cart-ref">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Tracking Id</p>
                        <p>{{$product->tracking_id}}</p>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6 c-cart-desc">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Description</p>
                        <p><a href="shop-product-details-2.html" class="c-font-bold c-theme-link c-font-dark">{{$product->category->title}}</a></p>
                    </div>
                    <div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-title">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Title</p>
                        <p class="c-cart-price c-font-bold">{{$product->title}}</p>
                    </div>
                    <div class="clearfix col-md-2 col-sm-3 col-xs-6 c-cart-price">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Price</p>
                        <p class="c-cart-price c-font-bold">{{$product->price}}</p>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-6 c-cart-qty">
                        <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Action</p>
                        <p><a class='btn btn-sm btn-outline blue table-row-edit' title='Edit' href='{{route("product_editing", $product->id)}}'> <i class='icon-note'></i>Edit</a></p>
                    </div>
                </div>
                @endforeach
                <!-- END: ORDER HISTORY ITEM ROW -->


            </div>

            <!-- END: CONTENT/SHOPS/SHOP-ORDER-HISTORY-2 -->
            <!-- END: PAGE CONTENT -->
        </div>
    </section>

</div>
<!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->

@endsection
