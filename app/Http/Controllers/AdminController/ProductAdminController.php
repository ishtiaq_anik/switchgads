<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use Auth;
use App\Product;
use App\ProductPicture;
use App\Http\Controllers\Controller;
use App\Http\Services\ProductService;
use App\Http\Services\EmailService;
use App\Http\Datatables\IphoneListingDatatable;
use Illuminate\Http\Request;

class ProductAdminController extends Controller
{
    protected $productService;
    public function __construct(ProductService $productService, EmailService $emailService)
    {
        $this->productService = $productService;
        $this->emailService = $emailService;
    }


    public function detail($product_id) {

        $product = Product::find($product_id);
        $subnav = $product->category['slug'];
            //dd($nav);
        $product_tracking_id = $product->tracking_id;
        $product_picture = ProductPicture::where('product_id', $product_tracking_id)->first();
        //        dd($product_picture);

        if ($product) {
            $project_path =  $this->getProjectUrl();
            $url = url('');
            $picture_thumbnail = $this->storage_link($project_path, 'uploads/thumbnail', $product->product_thumb);

            $product_picture1= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'1/thumbnail', $product_picture['product_picture_1']);
            $product_picture2= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'2/thumbnail', $product_picture['product_picture_2']);
            $product_picture3= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'3/thumbnail', $product_picture['product_picture_3']);

            $product_picture1_original= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'1/original', $product_picture['product_picture_1']);
            $product_picture2_original= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'2/original', $product_picture['product_picture_2']);
            $product_picture3_original= $this->storage_link($project_path, 'picture/'.$product_picture['product_id'].'3/original', $product_picture['product_picture_3']);

            
            //    dd($product->status);
            $product_statuses = $this->getProductStatus($product->status);
            //    dd($product_statuses['current']['specific_title']);
            $label = $product_statuses['current']['label'];
            $title = $product_statuses['current']['title'];


            $params = [
                'product'                             => $product,
                'product_number'                      => $product->id,
                'product_picture1'                    => $product_picture1,
                'product_picture2'                    => $product_picture2,
                'product_picture3'                    => $product_picture3,
                'product_picture1_original'           => $product_picture1_original,
                'product_picture2_original'           => $product_picture2_original,
                'product_picture3_original'           => $product_picture3_original,
                'product_thumbnail'                   => $picture_thumbnail,
                'status'                              => $product_statuses,
                'label'                               => $label,
                'title'                               => $title,
            ];
            return view('admin-pages.product.products-detail', $params)->withNav('listed-products')->withSubnav($subnav);
        }
        return redirect(route($this->nav));
    }



    public function statusUpdate(Request $request) {

        //    var_dump($request->all()); die();
        $new_status = $request->input('status');

        $product = Product::find((int)$request->id);

        $user = Auth::user();


        $previous_status = $product->status;
        $product->status = $new_status;

        if ($product->save()) {
            if($new_status == "verification-done-approved"){

                if($product->email) {
                    $emailService = new EmailService;
                    $emailService->sendApprovalEmail($product->id);
                }
            }elseif ($new_status == "verification-done-not-approved") {
                if($product->email) {
                    $emailService = new EmailService;
                    $emailService->sendNotApprovalEmail($product->id);
                }
            }
            $this->success();
            $this->message = ["Status Changed Successfully"];
            $data = $this->getProductStatus($product->status);
            // $data['redirect_to'] = route('certificates');
        }else {
            $this->message =[];
        }

        $this->data = isset($data) ? $data : [];
        return $this->output();
    }


}
