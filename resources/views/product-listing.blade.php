@extends('layouts.master')

@section('title', 'Product Listing')

@section('page_style_one')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/assets/admin/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/default/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>

@endsection

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->


<section>
    <div class="c-content-box c-size-md c-bg-white">

        <div class="container">

            <form class="form-horizontal" id="ajaxFormMain" enctype='multipart/form-data'>
                <input id="product_category" data-title="create" type="hidden" name="product_category" value="{{$productCategory}}"
                data-action="{{route('product_model_load')}}" data-target="#selectProductModel"
                data-action-1="{{route('product_accessory_load')}}" data-target-1="#selectAccessory" onkeypress="">
                {{ csrf_field() }}
                {{ ajax_action($route) }}
                {{ ajax_method("post") }}
                <div class="row">
                    <div class="col-md-7">
                        <!-- payment Info -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label  c-font-uppercase">payment Info</div>
                            <div class="c-body">

                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Ask Price</h4>
                                    <p class="c-font-thin c-font-14">What is the asking amount your wanting for your gadget? Include shipping costs in price. SwitchGads purchase fees are paid by the buyer.</p>

                                </div>
                                <div class="form-group has-error-price">
                                    <div class="col-md-3">
                                        <input id="price" name="price" class="form-control  c-square c-theme" type="number" placeholder="10$">
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>
                                    <div class="col-md-7">
                                        <p class="c-font-15">+ $5 (Purchase Fee) =  <strong id="base_price">15</strong> (Total Price shown to Buyers)</p>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">paypal email</h4>
                                    <p class="c-font-thin c-font-14">The PayPal address you will use to accept payment for your gadgets. (Not shown publicly)</p>

                                </div>
                                <div class="form-group has-error-email">
                                    <div class="col-md-10">
                                        <input name="email" class="form-control  c-square c-theme" type="email" placeholder="">
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- description -->
                        <div class="c-content-panel c-panel-border">
                            <div class="c-label  c-font-uppercase">product description</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Product Model</h4>
                                    <p class="c-font-thin c-font-14">Tell the buyer about your device's model</p>

                                </div>
                                <div class="form-group has-error-model_id ">
                                    <div class="col-md-10">
                                        <select id="selectProductModel" class="form-control  c-square c-theme" name="model_id">
                                            <option value="0">Select Model</option>
                                            @if($productTypes)
                                            @foreach($models as $model)
                                            <option value="{{ $model->id }}" <?php if($model->model_id == $model->id){echo "selected";}?> >
                                                {{ $model->title }}
                                            </option>
                                            @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">headline</h4>
                                    <p class="c-font-thin c-font-14">Tell the buyer a little about your device</p>

                                </div>
                                <div class="form-group has-error-title">
                                    <div class="col-md-10">
                                        <input name="title" class="form-control  c-square c-theme" type="text" placeholder="">
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">description</h4>
                                    <p class="c-font-thin c-font-14">A brief description of your device and listing.</p>

                                </div>
                                <div class="form-group has-error-description">
                                    <div class="col-md-10">
                                        <textarea name="description"  class="form-control  c-square c-theme" rows="5"></textarea>
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- Condition -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label  c-font-uppercase">Condition controls</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Overall Device Condition *</h4>
                                    <p class="c-font-thin c-font-14">Honesty rules! Liars get Booed!</p>

                                </div>

                                <div class="form-group has-error-condition c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-list">
                                            @foreach($conditions as $condition)
                                            <div class="c-radio has-success">
                                                <input type="radio" class="c-radio" value="{{$condition->id}}" id="inputCondition{{$condition->title}}" name="condition">
                                                <label for="inputCondition{{$condition->title}}">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    {{ $condition->title }}
                                                </label>

                                            </div>
                                            <div class=" c-title-md clearfix">
                                                <p class="c-font-thin c-font-14">Honesty rules! Liars get Booed!</p>
                                            </div>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Repaired or Refurbished ?</h4>
                                    <p class="c-font-thin c-font-14">Has this device ever been opened to be repaired?</p>

                                </div>
                                <div class="form-group has-error-is_repaired c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input type="radio" id="repairedYes" class="c-radio" value="yes" name="is_repaired">
                                                <label for="repairedYes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input type="radio" id="repairedNo" class="c-radio" value="no" name="is_repaired">
                                                <label for="repairedNo">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Damage? Repaired ?</h4>
                                    <p class="c-font-thin c-font-14">Talk to us. What repairs or damage has this device seen?</p>

                                </div>
                                <div class="form-group has-error-damage_description">
                                    <textarea name="damage_description" class="form-control  c-square c-theme" rows="5"></textarea>
                                    <span class="help-block hide">A block of help text.</span>
                                </div>



                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Are you the OG (Original Owner) ?</h4>
                                    <p class="c-font-thin c-font-14">Select no if you did not buy from a big chain retailer.</p>

                                </div>
                                <div class="form-group has-error-original_owner c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input  type="radio" id="originalOwnerYes" class="c-radio" value="yes" name="original_owner">
                                                <label for="originalOwnerYes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input  type="radio" id="originalOwnerNo" class="c-radio" value="no"  name="original_owner">
                                                <label for="originalOwnerNo">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Date Expires</h4>
                                    <p class="c-font-thin c-font-14">When do you want your listing to expire ?  *Hint* You can extend your date when you edit your listing.</p>

                                </div>

                                <div class="form-group has-error-date_expire">
                                    <div class="col-md-10">
                                        <div class="input-group input-medium date date-picker" data-date-format="dd/mm/yyyy" data-rtl="false" data-date-end-date="0d">
                                            <input type="text" class="form-control c-square c-theme" name="date_expire" placeholder="dd/mm/yyyy" autocomplete="false"
                                            value="">
                                            <span class="input-group-btn">
                                                <button class="btn default c-btn-square" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Device Information -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label  c-font-uppercase">Device Information</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <p class="c-font-thin c-font-14">I need help finding my ESN-IMEI-MEID number.</p>

                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Device IMEI (HEX)/Serial No*</h4>
                                    <p class="c-font-thin c-font-14">IMEI numbers are used to make sure the device has not been reported lost or stolen. IMEI numbers should be 15 digits long.</p>

                                </div>
                                <div class="form-group has-error-imei">
                                    <div class="col-md-10">
                                        <input name="imei" class="form-control  c-square c-theme" type="text" placeholder="">
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Device Color *</h4>
                                    <p class="c-font-thin c-font-14">Pick your color!</p>

                                </div>
                                <div class="form-group has-error-color_id ">
                                    <div class="col-md-10">
                                        <select class="form-control  c-square c-theme" name="color_id">
                                            <option value="-1">Select Color</option>
                                            @foreach($colors as $color)
                                            <option value="{{ $color->id }}" <?php if($color->color_id == $color->id){echo "selected";}?> >
                                                {{ $color->title }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Device Storage: *</h4>
                                    <p class="c-font-thin c-font-14">Pick your storage!</p>

                                </div>
                                <div class="form-group has-error-storage_id ">
                                    <div class="col-md-10">
                                        <select name="storage_id" class="form-control  c-square c-theme">
                                            <option value="-1">Select Storage</option>
                                            @foreach($storages as $storage)
                                            <option value="{{ $storage->id }}" <?php if($storage->storage_id == $storage->id){echo "selected";}?> >
                                                {{ $storage->title }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Device Mods -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label  c-font-uppercase">Device Mods</div>
                            <div class="c-body">
                                <div class="alert alert-danger" role="alert"><strong>Has the device been rooted, jailbroken, unlocked, or otherwise modified? </strong><br>If you're not sure, you can probably skip this section.</div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Is device unlocked?</h4>
                                    <p class="c-font-thin c-font-14">Unlocked = Can use on any compatible carrier services.</p>

                                </div>
                                <div class="form-group has-error-is_locked c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input type="radio" id="locked-yes" class="c-radio" value="yes" name="is_locked">
                                                <label for="locked-yes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input type="radio" id="locked-no" class="c-radio" value="no" name="is_locked">
                                                <label for="locked-no">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Is this a rooted device?</h4>
                                    <p class="c-font-thin c-font-14 hide">Has the device been rooted? (If so, please describe below).</p>

                                </div>
                                <div class="form-group has-error-is_rooted c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input type="radio" id="rooted-yes" class="c-radio" value="yes" name="is_rooted">
                                                <label for="rooted-yes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input type="radio" id="rooted-no" class="c-radio" value="no" name="is_rooted">
                                                <label for="rooted-no">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">MOD / ROM Description</h4>
                                    <p class="c-font-thin c-font-14">If you've loaded another ROM or MOD, please describe it here.</p>

                                </div>
                                <div class="form-group has-error-mod_description">
                                    <div class="col-md-10">
                                        <textarea name="mod_description" class="form-control  c-square c-theme" rows="5"></textarea>
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Device accessories -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label  c-font-uppercase">Device accessories</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Included Accessories</h4>
                                    <p class="c-font-thin c-font-14 ">Please check the accessories you will be including. The device's battery is required.</p>

                                </div>

                                <div class="form-group has-error-accessories c-checkboxes">
                                    <div class="col-md-12">
                                        <div class="c-checkbox-inline" id="selectAccessory">

                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Accessories Description</h4>
                                    <p class="c-font-thin c-font-14">What accessories come with the device?</p>

                                </div>

                                <div class="form-group has-error-accessories_description">
                                    <div class="col-md-10">
                                        <textarea name="accessories_description" class="form-control  c-square c-theme" rows="5"></textarea>
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Shipping accessories -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label  c-font-uppercase">Shipping & handeling</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Shipping Internationally ?</h4>
                                    <p class="c-font-thin c-font-14">If so, put it in the details below.</p>

                                </div>
                                <div class="form-group has-error-is_shipping c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input type="radio" id="shipping-yes" class="c-radio" value="yes" name="is_shipping">
                                                <label for="shipping-yes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input type="radio" id="shipping-no" class="c-radio" value="no" name="is_shipping">
                                                <label for="shipping-no">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Country *</h4>
                                    <p class="c-font-thin c-font-14">The country this listing is posted from.</p>

                                </div>

                                <div class="form-group has-error-shipping_country ">
                                    <div class="col-md-10">
                                        <select name="shipping_country" class="form-control  c-square c-theme">
                                            <option value="">Country...</option>
                                            <option value="1">Afghanistan</option>
                                            <option value="2">Albania</option>
                                            <option value="3">Australia</option>
                                            <option value="7">Austria</option>
                                            <option value="8">Azerbaijan</option>
                                            <option value="9">Bahamas</option>
                                            <option value="4">Bahrain</option>
                                            <option value="5">Bangladesh</option>
                                            <option value="6">Barbados</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Ship From Location *</h4>
                                    <p class="c-font-thin c-font-14">The location (e.g. city and state) you will ship from.</p>

                                </div>
                                <div class="form-group has-error-shipping_location">
                                    <div class="col-md-10">
                                        <input name="shipping_location" class="form-control  c-square c-theme" type="text" placeholder="">
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">
                                        Do you want the PayPal address to be confirmed ?
                                    </h4>
                                    <p class="c-font-thin c-font-14">Do you want a confirmed PayPal address from buyer? If so, seller protection is better but sale opportunities might decrease.</p>

                                </div>
                                <div class="form-group has-error-paypal_confirm c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input type="radio" id="paypal-yes" class="c-radio" value="yes" name="paypal_confirm">
                                                <label for="paypal-yes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input type="radio" id="paypal-no" class="c-radio" value="no" name="paypal_confirm">
                                                <label for="paypal-no">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Shipping Description</h4>
                                    <p class="c-font-thin c-font-14">Describe the shipping options you will use to ship to the buyer. Please include the carrier (e.g. FedEx, UPS, USPS) and service (e.g. standard ground, two-day, overnight). Remember, seller pays standard shipping costs.</p>

                                </div>

                                <div class="form-group has-error-shipping_description">
                                    <div class="col-md-10">
                                        <textarea name="shipping_description" class="form-control  c-square c-theme" rows="5"></textarea>
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>

                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Standard shipping policies:</h4>
                                    <ul class="c-content-list-1 c-theme c-separator-dot c-font-14" >
                                        <li>Sellers are expected to ship within two business days of receiving payment.</li>
                                        <li>Standard shipping costs (ground shipping with tracking and insurance in the same country) should be.</li>
                                        <li>Included in listed price.</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <!-- Returns / Refunds -->
                        <div class="c-content-panel  c-panel-border">
                            <div class="c-label c-font-uppercase">Returns / Refunds</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Returns allowed</h4>
                                    <p class="c-font-thin c-font-14">Please describe your additional return policy.</p>

                                </div>
                                <div class="form-group has-error-returns_allowed c-radios">
                                    <div class="col-md-10">
                                        <div class="c-radio-inline">
                                            <div class="c-radio">
                                                <input type="radio" id="return-yes" class="c-radio" value="yes" name="returns_allowed">
                                                <label for="return-yes">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    yes
                                                </label>
                                            </div>
                                            <div class="c-radio">
                                                <input type="radio" id="return-no" class="c-radio" value="no" name="returns_allowed">
                                                <label for="return-no">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                    no
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">return Description</h4>
                                    <p class="c-font-thin c-font-14">Specify in detail if you will accept returns.How the refund process will work.</p>

                                </div>
                                <div class="form-group has-error-return_description">
                                    <div class="col-md-10">
                                        <textarea name="return_description" class="form-control  c-square c-theme" rows="5"></textarea>
                                        <span class="help-block hide">A block of help text.</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-5">
                        <div class="c-content-panel">
                            <div class="c-label c-font-uppercase">Product Image</div>
                            <div class="c-body">
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Product Thumbnail</h4>
                                    <p class="c-font-thin c-font-14">If so, put it in the details below.</p>

                                </div>
                                <div class="form-group has-error-picture">
                                    <div class="col-md-12">
                                        <div class="fileinput  fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control c-square c-theme uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="picture">
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
                                            </div>
                                            <p class="help-block">.jpeg, .bmp, .png (max 2Mb)</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="c-content-title-1 c-title-md c-margin-b-20 clearfix">
                                    <h4 class="c-left c-font-uppercase c-font-bold">Product Image</h4>
                                    <p class="c-font-thin c-font-14">Please upload your devices recent & clear picture.</p>

                                </div>
                                <div class="form-group has-error-picture_1">
                                    <div class="col-md-12">
                                        <div class="fileinput  fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control c-square c-theme uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="picture_1">
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
                                            </div>
                                            <p class="help-block">.jpeg, .bmp, .png (max 2Mb)</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-error-picture_2">
                                    <div class="col-md-12">
                                        <div class="fileinput  fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control c-square c-theme uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="picture_2">
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
                                            </div>
                                            <p class="help-block">.jpeg, .bmp, .png (max 2Mb)</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-error-picture_3">
                                    <div class="col-md-12">
                                        <div class="fileinput  fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control c-square c-theme uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="picture_3">
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">Remove </a>
                                            </div>
                                            <p class="help-block">.jpeg, .bmp, .png (max 2Mb)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group c-margin-t-40">
                            <div class="col-sm-offset-5 col-md-7">
                                <button type="submit" id="form-submit" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                                <!-- <button type="submit" class="btn btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</section>
@endsection

@section('page_script_two')

<script src="{{ asset('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<!-- <script src="{{ asset('assets/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script> -->
@endsection
@section('page_script_three')
<script src="{{ asset('assets/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/frontend/js/datepicker.js') }}" type="text/javascript"></script>

<script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
<!-- <script src="{{ asset('assets/admin/pages/scripts/profile.min.js') }}" type="text/javascript"></script> -->

<script src="{{asset('js/apply.js')}}" type="text/javascript"></script>


@endsection
