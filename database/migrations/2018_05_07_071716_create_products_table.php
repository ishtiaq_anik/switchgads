<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tracking_id')->nullable();
            $table->string('title')->nullable();
            $table->string('email')->nullable();
            $table->text('description')->nullable();
            $table->text('damage_description')->nullable();
            $table->text('mod_description')->nullable();
            $table->text('accessories_description')->nullable();
            $table->text('shipping_description')->nullable();
            $table->text('return_description')->nullable();
            $table->integer('category_id')->index()->nullable()->unsigned();
            $table->integer('model_id')->index()->nullable()->unsigned();
            $table->integer('storage_id')->index()->nullable()->unsigned();
            $table->integer('color_id')->index()->nullable()->unsigned();
            $table->integer('condition_id')->index()->nullable()->unsigned();
            $table->integer('user_id')->index()->nullable()->unsigned();
            $table->text('accessories')->nullable();
            $table->integer('shipping_country')->index()->nullable()->unsigned();
            $table->string('is_repaired')->nullable();
            $table->string('is_locked')->nullable();
            $table->string('is_rooted')->nullable();
            $table->string('original_owner')->nullable();
            $table->string('shipping_international')->nullable();
            $table->string('paypal_confirmed')->nullable();
            $table->string('returns_allowed')->nullable();
            $table->string('product_active')->nullable();
            $table->date('expire_date')->nullable();
            $table->string('imei')->nullable();
            $table->string('shipping_location')->nullable();
            $table->string('product_thumb')->nullable();
            $table->string('privacy_policy')->nullable();
            $table->integer('price')->nullable();
            $table->string('status')->nullable()->default("active");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
