<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('user_id')->index();
            $table->integer('mobile_category_id')->index();
            $table->integer('mobile_color_id')->index();
            $table->integer('mobile_carrier_id')->index()->nullable();
            $table->integer('mobile_storage_id')->index();
            $table->integer('mobile_condition_id')->index();
            $table->text('shipping_description');
            $table->text('condition_description');
            $table->text('option_description');
            $table->string('price');
            $table->string('code');
            $table->string('model');
            $table->string('slug')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobiles');
    }
}
