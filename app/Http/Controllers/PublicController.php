<?php

namespace App\Http\Controllers;

use App\Models\Product\Category;
use App\User;
use Illuminate\Http\Request;

class PublicController extends Controller {

    public function index(){
        //    $productTypes = ProductCategory::all();

        $params = [
            'productTypes' => $this->productTypes,
        ];

        return view('home',$params)->withNav('home');
    }
    



    public function policy() {
        return view('privacy-policy');
    }





}
