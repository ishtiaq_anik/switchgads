<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Certificate;
use File;
use Illuminate\Support\Facades\Storage;

class EmailForgetTrackingId extends Mailable
{
    use Queueable, SerializesModels;

    public $id;
    public $certificate;
    public $applicant_name;
    public $tracking_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->certificate = Certificate::find($this->id);
        $this->applicant_name = $this->certificate->name;
        $this->tracking_id = $this->certificate->tracking_id;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Rangamati Permanent Resident Certificate System')->markdown('emails.certificate.email_forget-trackingid');
    }
}
