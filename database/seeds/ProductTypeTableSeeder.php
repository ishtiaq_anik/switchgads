
<?php

use Illuminate\Database\Seeder;

class ProductTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $type = [];
         for($i=0;$i<6;$i++){
             $type[]=[
                 'title' => 'Type-' . $i
             ];

         }
         \App\Models\Product\ProductCategory::insert($type);
     }
}
