@extends('layouts.master')

@section('title', 'Product Details')

@section('content')
<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->

<section>
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordegreen1 c-bordegreen1-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Profile Dashboard</h3>
                <h4 class=""></h4>
            </div>

        </div>
    </div>

</section>
<div class="container">
    <section>

        @include('layouts.side-menu')

    </section>
    <section>
        <div class="c-layout-sidebar-content ">
            <!-- BEGIN: PAGE CONTENT -->
            <div class="c-content-title-1">
                <h3 class="c-font-uppercase c-font-bold">Edit Profile</h3>
                <div class="c-line-left"></div>
            </div>
            <form class="form-horizontal" id="ajaxFormMain" enctype='multipart/form-data'>
                {{ csrf_field() }}
                {{ ajax_action($route) }}
                {{ ajax_method("post") }}
                <!-- BEGIN: ADDRESS FORM -->
                <div class="">
                    <!-- BEGIN: BILLING ADDRESS -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6 has-error-firstname">
                                    <label class="control-label">First Name </label>
                                    <input name="firstname" type="text" class="form-control c-square c-theme" placeholder="First Name">
                                </div>
                                <div class="col-md-6 has-error-lastname">
                                    <label class="control-label">Last Name</label>
                                    <input name="lastname" type="text" class="form-control c-square c-theme" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6 has-error-email">
                                    <label class="control-label">Email Address</label>
                                    <input name="email" type="email" class="form-control c-square c-theme" placeholder="Email Address">
                                </div>
                                <div class="col-md-6 has-error-paypal_email">
                                    <label class="control-label has-error-description">Phone</label>
                                    <input name="paypal_email" type="tel" class="form-control c-square c-theme" placeholder="Phone">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6 has-error-country">
                                    <label class="control-label">State / County</label>
                                    <select name="country" class="form-control c-square c-theme">
                                        <option value="0">Select an option...</option>
                                        <option value="1">Malaysia</option>
                                        <option value="2">Singapore</option>
                                        <option value="3">Indonesia</option>
                                        <option value="4">Thailand</option>
                                        <option value="5">China</option>
                                    </select>
                                </div>
                                <div class="col-md-6 has-error-location">
                                    <label class="control-label">Location</label>
                                    <input name="location" type="text" class="form-control c-square c-theme" placeholder="Town / City">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- END: BILLING ADDRESS -->
                    <!-- BEGIN: PASSWORD -->
                    <div class="row">
                        <div class="form-group col-md-12 has-error-password">
                            <label class="control-label">Change Password</label>
                            <input type="password" class="form-control c-square c-theme" placeholder="Password">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 has-error-confirm_password">
                            <label class="control-label">Repeat Password</label>
                            <input name="confirm_password" type="password" class="form-control c-square c-theme" placeholder="Password">
                            <p class="help-block">Hint: The password should be at least six characters long. <br>
                                To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ &amp; ).</p>
                            </div>
                        </div>
                        <!-- END: PASSWORD -->
                        <div class="row c-margin-t-30">
                            <div class="form-group col-md-12" role="group">
                                <button type="submit" id="form-submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Submit</button>
                                <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <!-- END: ADDRESS FORM -->
                </form>			<!-- END: PAGE CONTENT -->
            </div>
        </section>

    </div>
    <!-- BEGIN: CONTENT/SHOPS/SHOP-2-2 -->


    @endsection

    @section('page_script_three')
<script src="{{ asset('assets/frontend/js/datepicker.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
    <!-- <script src="{{ asset('assets/admin/pages/scripts/profile.min.js') }}" type="text/javascript"></script> -->

    <script src="{{asset('js/apply.js')}}" type="text/javascript"></script>

    @endsection
