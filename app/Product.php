<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function category(){

        return $this->belongsTo('App\Models\Product\ProductCategory','category_id');
    }
    public function color(){

        return $this->belongsTo('App\MobileColor','color_id');
    }
    public function model(){

        return $this->belongsTo('App\Models\Product\ProductModel','model_id');
    }
    public function storage(){

        return $this->belongsTo('App\MobileStorage','model_id');
    }
    public function condition(){

        return $this->belongsTo('App\MobileCondition','condition_id');
    }
    public function accessories(){

        return $this->belongsTo('App\MobileAccessory','accessories');
    }
    public function seller(){
        return $this->belongsTo('App\User','user_id');
    }



}
