<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Models\Product\ProductCategory;
use App\SubproductCategory;
use App\Http\Controllers\Controller;
use App\Http\Datatables\ProductCategoryDatatable;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('product_type_index'),
            'dataload_url' => route('product_type_dataload'),
            'title' => "Product Category",
            'titles' => "Product Categories",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('product-type');
    }

    public function datatable(Request $request) {
        $ajax_table = new ProductCategoryDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $productCategory = new ProductCategory;
        $title = 'Product Type';
        $params = [
            'productCategory' => $productCategory,
            'route'    => route('product_type_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.product.product-category-create', $params)->withNav('product-type');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {


        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:10|unique:categories,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        // $productCategory = ProductCategory::find($request->_ajax_id);
        if ($request->_ajax_id) {
            $productCategory = ProductCategory::find($request->_ajax_id);
        } else {
            $productCategory = new ProductCategory;
        }

        $productCategory->title = $request->input('title');
        $productCategory->slug = $request->input('slug');
        if($productCategory->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('product_type_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\ProductCategory  $productCategory
    * @return \Illuminate\Http\Response
    */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\ProductCategory  $productCategory
    * @return \Illuminate\Http\Response
    */
    public function edit($productCategory_id)
    {
        $productCategory = ProductCategory::find($productCategory_id);
        if($productCategory){
            $title = 'productCategory';
            $params = [
                'productCategory' => $productCategory,
                'route'    => route('product_type_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.product.product-category-create', $params);
        }
        return redirect()->route('product_type_index')->withNav('product-type');
    }


    // public function subcategories(Request $request) {
    //     $id = isset($request->id) ? $request->id : "";
    //     $subcategories = SubproductCategory::whereProductCategoryId($id)->get();
    //     if($subcategories) {
    //         $this->success();
    //         $this->message = "records found!";
    //         $this->data = $subcategories;
    //     }
    //     return $this->output();
    // }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\ProductCategory  $productCategory
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, ProductCategory $productCategory)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\ProductCategory  $productCategory
    * @return \Illuminate\Http\Response
    */
    public function destroy(ProductCategory $productCategory)
    {
        //
    }
}
