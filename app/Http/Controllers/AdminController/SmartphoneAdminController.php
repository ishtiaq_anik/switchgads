<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Services\ProductService;
use App\Http\Datatables\SmartphoneListingDatatable;
use Illuminate\Http\Request;

class SmartphoneAdminController extends Controller
{
    public function smartphoneListing()
    {
        $params = [
            'base_url' => route('smartphone_index'),
            'dataload_url' => route('smartphone_dataload'),
            'title' => "Smartphone",
            'titles' => "Smartphones",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Model", "filter" => $this->filterText("model")],
                [ "title" => "Status", "filter" => ""],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('listed-products')->withSubnav('smartphones');
    }

    public function datatable(Request $request) {
        $ajax_table = new SmartphoneListingDatatable;
        return $ajax_table->table($request);
    }






}
