<?php

namespace App\Http\Controllers\AdminController;

use Validator;
use App\Category;
use App\Subcategory;
use App\Http\Datatables\CategoryDatatable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $params = [
            'base_url' => route('category_index'),
            'dataload_url' => route('category_dataload'),
            'title' => "Category",
            'titles' => "Categories",
            'icon' => "icon-layers",
            'icons' => "icon-layers",
            'create' => 'create',
            'filter' => true,
            'unsortable' => "0",
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Title", "filter" => $this->filterText("title")],
                [ "title" => "Slug", "filter" => $this->filterText("slug")],
                [ "title" => "updated_time", "filter" => $this->filterDateRange()],
                [ "title" => "action", "filter" => $this->filterAction()],
            ],
        ];

        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        // dd($params);
        return view('admin.table', $params)->withNav('photos')->withSubnav('categories');
    }

    public function datatable(Request $request) {
        $ajax_table = new CategoryDatatable;
        return $ajax_table->table($request);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $category = new Category;
        $title = 'category';
        $params = [
            'category' => $category,
            'route'    => route('category_store'),
            'method'   => 'post',
            'title'    => $title,
        ];
        return view('admin-pages.photos.category-create', $params)->withNav('photos')->withSubnav('categories');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {


        $validator_data = [
            'title'     => 'required|max:255',
            'slug'   => 'required|max:10|unique:categories,slug',
        ];
        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            $this->message = "Please Fill All The Field";
            return $this->validationError();
        }
        // $category = Category::find($request->_ajax_id);
        if ($request->_ajax_id) {
            $category = Category::find($request->_ajax_id);
        } else {
            $category = new Category;
        }

        $category->title = $request->input('title');
        $category->slug = $request->input('slug');
        if($category->save()){
            $this->success();
            $default = "";
            $this->message = [];
            $data['redirect_to'] = route('category_index');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Category  $category
    * @return \Illuminate\Http\Response
    */
    public function show(Category $category)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Category  $category
    * @return \Illuminate\Http\Response
    */
    public function edit($category_id)
    {
        $category = Category::find($category_id);
        if($category){
            $title = 'category';
            $params = [
                'category' => $category,
                'route'    => route('category_store'),
                'method'   => 'post',
                'title'    => $title,
            ];
            return view('admin-pages.photos.category-create', $params)->withNav('photos')->withSubnav('categories');
        }
        return redirect()->route('category_index')->withNav('photos')->withSubnav('categories');
    }


    public function subcategories(Request $request) {
        $id = isset($request->id) ? $request->id : "";
        $subcategories = Subcategory::whereCategoryId($id)->get();
        if($subcategories) {
            $this->success();
            $this->message = "records found!";
            $this->data = $subcategories;
        }
        return $this->output();
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Category  $category
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Category  $category
    * @return \Illuminate\Http\Response
    */
    public function destroy(Category $category)
    {
        //
    }
}
