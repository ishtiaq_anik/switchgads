<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Request;
use \App\Role;

class RolesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $owner = new Role();
        $owner->name         = 'seller';
        $owner->display_name = 'Seller'; // optional
        $owner->description  = 'Seller is the owner of product'; // optional
        $owner->save();

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();

        $admin = new Role();
        $admin->name         = 'buyer';
        $admin->display_name = 'Buyer'; // optional
        $admin->description  = 'User is allowed to visit and buy product'; // optional
        $admin->save();

    }

}
