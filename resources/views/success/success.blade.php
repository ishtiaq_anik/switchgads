@extends('layouts.master')
@section('title', 'Congrats')
@section('content')

<section>
    <div class="c-content-box c-size-sm c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="c-content-panel">
                        <div class="c-label">without dot</div>
                        <div class="c-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="c-content-title-2">
                                        <h3 class="c-center">Congrats!</h3>
                                        <div class="c-line c-theme-bg c-theme-bg-after"></div>
                                        <p class="c-center">Your product submitted successfully. Now your product's details is in verification process. Thank You.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
