
<?php

use Illuminate\Database\Seeder;

class MobileAccessoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $accessories = [];
         for($i=0;$i<6;$i++){
             $accessories[]=[
                 'title' => 'Accessories-' . $i
             ];

         }
         \App\MobileAccessory::insert($accessories);
     }
}
