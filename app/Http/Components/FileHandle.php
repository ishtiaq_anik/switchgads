<?php

namespace App\Http\Components;
use Image;
use File;
use Illuminate\Support\Facades\Storage;

trait FileHandle {

    public function random_str($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
    /*
    * save file in Storage
    */
    public function saveInStorage($file, $folder){

        $tracking_id = $this->random_str(6);
           $fileName = $tracking_id.'.'.$file->getClientOriginalExtension();
    //    $fileName = $tracking_id.'.'.$file->getClientOriginalExtension();
        //$fileName = time().'.'.$file->getClientOriginalExtension();
        $fl = Image::make($file->getRealPath());
        $fl->resize(120, 120, function ($constraint) {
            $constraint->aspectRatio();
        });
        $fl->stream();

        Storage::disk('local')->put('public/'.$folder.'//thumbnail//'.$fileName, $fl, 'public');
        $f2 = Image::make($file->getRealPath());
        $f2->resize(1200, 900, function ($constraint) {
            $constraint->aspectRatio();
        });
        $f2->stream();
        Storage::disk('local')->put('public/'.$folder.'//original//'.$fileName, $f2, 'public');

        return $fileName;
    }
    /*
    * Certificate Status Conversion
    */
    /*
    * save itinerary file in Storage
    * mimes: jpg,jpeg,pdf
    */
    public function storePdfInStorage($file,$folder){
        if($file){
            //    $tracking_id = $this->random_str(6);
            //    $fileName = $tracking_id.'.'.$file->getClientOriginalExtension();
            //    dd($fileName);
            $fileName = time().'.'.$file->getClientOriginalExtension();
            Storage::putFileAs('public/'.$folder.'//original/', $file, $fileName);
            Storage::putFileAs('public/'.$folder.'//thumbnail/', $file, $fileName);
            return $fileName;
        }else{
            return "";
        }
    }

    public function checkFileExtension($file){
        $extensions = ["jpg","jpeg","png","bmp","gif","pdf"];
        if(in_array($file->getClientOriginalExtension(),$extensions)){
            return $file->getClientOriginalExtension();
        }
        return "";
    }

    public function uploadFiles($file,$folder){
        $filename = "";
        if($this->checkFileExtension($file)!=""){
            if($this->checkFileExtension($file)=="pdf"){
                $filename = $this->storePdfInStorage($file,$folder);
            }else{
                $filename = $this->saveInStorage($file,$folder);
            }
        }
        return $filename;
    }



}
