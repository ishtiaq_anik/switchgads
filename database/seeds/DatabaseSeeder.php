<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
       $this->call(UsersTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(ProductTypeTableSeeder::class);
       $this->call(CategoryTableSeeder::class);
        $this->call(MobileConditionTableSeeder::class);
        $this->call(MobileStorageTableSeeder::class);
        $this->call(MobileColorTableSeeder::class);
       $this->call(MobileAccessoryTableSeeder::class);

    }
}
