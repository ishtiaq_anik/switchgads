@extends('layouts.error')

@section('title', 'Product Details')

@section('content')

    <section>
        <div class="c-content-box c-size-sm c-bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>Sorry! You are not authorized to access this </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

