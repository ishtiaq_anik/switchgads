@extends('admin.master')

@section('title', 'Detail')

@section('page_style_plugin')
<link href="/css/profile.css" rel="stylesheet" type="text/css" />
<link href="/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Product Details
                    <small></small>
                </h1>
            </div>
        </div>
        <!-- END PAGE TITLE -->

        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">Dashboard</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('dashboard') }}">Products</a>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->


        <div class="page-content-col">
            <div class="row">
                <div class="col-md-4">
                    <div class="portlet light profile-sidebar-portlet bordered">

                        <div class="profile-userpic">
                            <img class="img-responsive" src="{{asset('assets/img/content/avatars/team1.jpg')}}"/>
                        </div>
                        <div class="profile-usertitle">
                            <div class="profile-usertitle"> <a class=" btn btn-circle green btn-sm" data-target="#picture-modal" data-toggle="modal">View</a> </div>
                            <div class="profile-usertitle"> {{$product->seller['name']}} </div>
                            <hr>

                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Info
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 value"> Product Type: </div>
                                <div class="col-md-7 "> {{ $product->category->title }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Product Title: </div>
                                <div class="col-md-7 "> {{ $product->title }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Product Description: </div>
                                <div class="col-md-7 "> {{ $product->description }}
                                </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 value"> Product Model: </div>
                                <div class="col-md-7 "> {{ $product->model->title }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Tracking Id: </div>
                                <div class="col-md-7 "> {{ $product['tracking_id'] }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Original Owner: </div>
                                <div class="col-md-7 "> {{ $product->original_owner }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Expire Date: </div>
                                <div class="col-md-7 "> {{ date('d M, Y', strtotime($product['expire_date'])) }} </div>
                            </div>

                            <div class="row static-info">
                                <div class="col-md-5 value"> Seller Price: </div>
                                <div class="col-md-7 ">${{ $product['price'] }} </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Payment Information: </div>
                                <div class="col-md-7 "> Paypal </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Product Info
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 value"> Product Condition: </div>
                                <div class="col-md-7 "> {{ $product->condition->title }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Product Repaired: </div>
                                <div class="col-md-7 "> {{ $product->is_repaired }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Damage Description: </div>
                                <div class="col-md-7 "> {{ $product['damage_description'] }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Accessories : </div>
                                <div class="col-md-7 "> {{ $product['accessories'] }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Accessories Description: </div>
                                <div class="col-md-7 "> {{ $product['accessories_description'] }}
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i> Basic Info
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 value"> IMEI: </div>
                                <div class="col-md-7 "> {{ $product->imei }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Color: </div>
                                <div class="col-md-7 "> {{ $product->color->title }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Storage: </div>
                                <div class="col-md-7 "> {{ $product->storage->title }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Locked: </div>
                                <div class="col-md-7 "> {{ $product->is_locked }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Rooted Device: </div>
                                <div class="col-md-7 "> {{ $product->is_rooted }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Mod Description: </div>
                                <div class="col-md-7 "> {{ $product->mod_description }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Location & Shipping Info
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 value"> Country: </div>
                                <div class="col-md-7 "> {{ $product->shipping_country }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> State: </div>
                                <div class="col-md-7 "> {{ $product['shipping_location'] }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Shipping International: </div>
                                <div class="col-md-7 "> {{ $product->shipping_international }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Shipping Descrption : </div>
                                <div class="col-md-7 "> {{ $product['shipping_description'] }}
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i> Return Policy
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row static-info">
                                <div class="col-md-5 value"> Return Allowed: </div>
                                <div class="col-md-7 "> {{ $product->returns_allowed }}
                                </div>
                            </div>
                            <div class="row static-info">
                                <div class="col-md-5 value"> Return Policy: </div>
                                <div class="col-md-7 "> {{ $product->return_description }}
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet green-meadow box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Location & Shipping Info
                            </div>

                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="form-horizontal form-info">

                                    <div class="col-md-4">
                                        <!--Certificate Birth Certificate-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label profile-usertitle-global"></span></label>
                                            <div class="col-md-8 text-center">
                                                <img src="{{$product_picture1}}" width="120" height="120"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">&nbsp;</span></label>
                                            <div class="col-md-8 text-center">
                                                <a class=" btn btn-circle green btn-sm" data-target="#picture1_modal" data-toggle="modal">View</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <!--Certificate Birth Certificate-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label profile-usertitle-global"></span></label>
                                            <div class="col-md-8 text-center">
                                                <img src="{{$product_picture2}}" width="120" height="120"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">&nbsp;</span></label>
                                            <div class="col-md-8 text-center">
                                                <a class=" btn btn-circle green btn-sm" data-target="#picture2_modal" data-toggle="modal">View</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <!--Certificate Chairman Certificate-->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label profile-usertitle-global"></span></label>
                                            <div class="col-md-8 text-center">
                                                <img src="{{$product_picture3}}" width="120" height="120"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">&nbsp;</span></label>
                                            <div class="col-md-8 text-center">
                                                <a class=" btn btn-circle green btn-sm" data-target="#picture3_modal" data-toggle="modal">View</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="portlet green-meadow box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Seller Info
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="row static-info">
                            <div class="col-md-3 value"> Paypal Email: </div>
                            <div class="col-md-9 "> {{ $product->email }}
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-3 value"> Paypal Confirmed: </div>
                            <div class="col-md-9 "> {{ $product['paypal_confirmed'] }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet green-meadow box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Action
                        </div>

                    </div>
                    <div class="portlet-body">
                        <span class="app-alert"></span>
                        <div class="text-center" >
                            <div class="btn-group" id="change_status">
                                <input type="hidden" name="ref_id" value="{{ $product->id }}">
                                <input type="hidden" name="status_update_url" value="{{ route('status_update')}}">
                                <a class="btn {{ $status['current']['color'] }}" href="javascript:;" data-toggle="dropdown">
                                    <i class="{{ $status['current']['icon_class'] }}"></i>

                                    <span class="hidden-xs"> {{ $status['current']['title'] }} </span>

                                    @if(isset($status['next']) and count($status['next']))
                                    <i class="fa fa-angle-down"></i>
                                    @endif
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    @foreach($status['next'] as $key => $status)
                                    <li>
                                        <a href="javascript:;" class="tool-action" data-value="{{ $key }}">

                                            <i class="{{ $status['icon_class'] }}"></i> {{ $status['title'] }}

                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
</div>

<div id="picture1_modal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Picture 1</h4>
            </div>
            <div class="modal-body text-center">
                <img alt="" src="{{ $product_picture1_original }}">
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-outline dark">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="picture2_modal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Picture 2</h4>
            </div>
            <div class="modal-body text-center">
                <img alt="" src="{{ $product_picture2_original }}">
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-outline dark">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="picture3_modal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Picture 3</h4>
            </div>
            <div class="modal-body text-center">
                <img alt="" src="{{ $product_picture3_original }}">
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-outline dark">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- See Report Note Modal -->


@endsection

@section('page_script_plugin')
<!-- <script src="{{ asset('/js/form.js') }}" type="text/javascript"></script> -->
<script src="{{ asset('/js/custom.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$("#download_visa").click(function() {
    var url = $(this).data("url")+'/original/'+$(this).data("file");
    console.log(url);
    var form = document.createElement("form");
    $("body").append(form);
    form.setAttribute('method',"get");
    form.setAttribute('action',url);
    form.submit();
});
</script>
@endsection
