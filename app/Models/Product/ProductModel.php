<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductModel extends Model
{

        use SoftDeletes;

        /**
        * The attributes that should be mutated to dates.
        *
        * @var array
        */

        protected $dates = ['deleted_at'];

        public function category(){
            return $this->belongsTo('App\Models\Product\ProductCategory','product_category_id');
        }

}
