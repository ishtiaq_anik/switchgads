<?php

use Illuminate\Database\Seeder;

class MobileColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         $color = [];
         for($i=0;$i<10;$i++){
             $color[]=[
                 'title' => 'Color' . $i
             ];

         }
         \App\MobileColor::insert($color);
     }
}
