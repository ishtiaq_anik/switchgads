<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application\ These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group\ Now create something great!
|
*/

Route::get('/', array('as'=>'home', function(){;
    if (\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->hasRole('admin')) {
        return App::make('\App\Http\Controllers\AdminController\DashboardController', [])->index();
        } else{
            return App::make('\App\Http\Controllers\ProductController', [])->home();
            }
            }));

            Route::get('/home', 'ProductController@home')->name('userhome');

            Route::get('/product-list/{slug}', 'ProductController@productList')->name('product_list');
            Route::get('/product-details/{slug}', 'ProductController@productDetails')->name('product_details');
            Route::get('/product-categories', 'ProductController@productCategories')->name('product_categories');
            // Route::get('/product-details/{slug}', 'ProductController@productLists')->name('product_lists');

            Route::group(['prefix' => 'product-categories'], function () {
                Route::get('/{slug}', 'ProductController@productCategoriesShow')->name('product_categories_show');
            });

            Route::group(['middleware' => ['auth', 'role:buyer|seller']], function() {

                Route::get('/product-sell', 'ProductController@productSell')->name('product_sell');
                Route::post('/product-category', 'ProductController@productCategorySend')->name('product_category_id');
                Route::post('/product-listing', 'ProductController@productListing')->name('product_listing');
                Route::get('/product-editing/{product_id}', 'ProductController@productListingEdit')->name('product_editing');
                Route::post('/product-listing/store', 'ProductController@productStore')->name('product_store');
                Route::post('/product-listing/load', 'ProductController@productModelLoad')->name('product_model_load');
                Route::post('/product-listing/accessory-load', 'ProductController@productAccessoryLoad')->name('product_accessory_load');
                Route::get('/success', 'ProductController@success')->name('success');

                //Route::post('/mobile_colors', 'ProductController@mobileColors')->name('mobile_color_load');

                Route::get('/user-dashboard', 'ProfileController@userProfileDashboard')->name('user_profile_dashboard');
                Route::get('/user-edit', 'ProfileController@userProfileEdit')->name('user_profile_edit');
                Route::post('/user-store', 'ProfileController@userStore')->name('user_profile_store');
                Route::get('/user-selling-history', 'ProfileController@userSellingHistory')->name('user_selling_history');

            });


            Route::group(['middleware' => ['auth', 'role:admin']], function() {

                /* Admin */
                //
                Route::get('/dashboard', 'AdminController\DashboardController@index')->name('dashboard');

                Route::group(['prefix' => 'product-listing'], function () {
                    Route::get('/iphone/index', 'AdminController\IphoneAdminController@iphoneListing')->name('iphone_index');
                    Route::post('/iphone/dataload', 'AdminController\IphoneAdminController@datatable')->name('iphone_dataload');
                    Route::get('/iphone/detail/{product}', 'AdminController\ProductAdminController@detail')->name('product_detail');
                    Route::post('/product-listing/status-update', 'AdminController\ProductAdminController@statusUpdate')->name('status_update');
                    // });
                    // Route::group(['prefix' => 'product-listing'], function () {
                    Route::get('/smartphone/index', 'AdminController\SmartphoneAdminController@smartphoneListing')->name('smartphone_index');
                    Route::post('/smartphone/dataload', 'AdminController\SmartphoneAdminController@datatable')->name('smartphone_dataload');
                    // Route::get('/smartphone/detail/{product}', 'AdminController\SmartphoneAdminController@detail')->name('smartphone_detail');
                });

                Route::group(['prefix' => 'users'], function () {
                    Route::get('/seller/index', 'AdminController\SellerAdminController@index')->name('seller_index');
                    Route::post('/seller/dataload', 'AdminController\SellerAdminController@datatable')->name('seller_dataload');
                    // });
                    // Route::group(['prefix' => 'product-listing'], function () {
                    Route::get('/buyer/index', 'AdminController\BuyerAdminController@smartphoneListing')->name('buyer_index');
                    Route::post('/buyer/dataload', 'AdminController\BuyerAdminController@datatable')->name('buyer_dataload');
                    // Route::get('/smartphone/detail/{product}', 'AdminController\SmartphoneAdminController@detail')->name('smartphone_detail');
                    Route::get('/seller/detail/{user}', 'AdminController\ProductAdminController@detail')->name('user_detail');
                    Route::post('/status-update', 'AdminController\ProductAdminController@statusUpdate')->name('user_status_update');
                });

                Route::group(['prefix' => 'product-type'], function () {

                    Route::get('/', 'AdminController\ProductCategoryController@index')->name('product_type_index');
                    Route::get('/create', 'AdminController\ProductCategoryController@create')->name('product_type_create');
                    Route::Post('/store', 'AdminController\ProductCategoryController@store')->name('product_type_store');
                    Route::get('/edit/{category_id}', 'AdminController\ProductCategoryController@edit')->name('product_type_edit');
                    Route::post('/dataload', 'AdminController\ProductCategoryController@datatable')->name('product_type_dataload');

                });

                Route::group(['prefix' => 'product-model'], function () {

                    Route::get('/', 'AdminController\ProductModelController@index')->name('product_model_index');
                    Route::get('/show', 'AdminController\ProductModelController@show')->name('product_model_show');
                    Route::get('/create', 'AdminController\ProductModelController@create')->name('product_model_create');
                    Route::post('/store', 'AdminController\ProductModelController@store')->name('product_model_store');
                    Route::get('/edit/{model_id}', 'AdminController\ProductModelController@edit')->name('product_model_edit');
                    Route::post('/dataload', 'AdminController\ProductModelController@datatable')->name('product_model_dataload');
                    Route::post('/productcategories', 'AdminController\ProductModelController@productCategories')->name('product_categories_load');

                });
                Route::group(['prefix' => 'product-accessory'], function () {

                    Route::get('/', 'AdminController\AccessoryController@index')->name('product_accessory_index');
                    Route::get('/show', 'AdminController\AccessoryController@show')->name('product_accessory_show');
                    Route::get('/create', 'AdminController\AccessoryController@create')->name('product_accessory_create');
                    Route::post('/store', 'AdminController\AccessoryController@store')->name('product_accessory_store');
                    Route::get('/edit/{accessory_id}', 'AdminController\AccessoryController@edit')->name('product_accessory_edit');
                    Route::post('/dataload', 'AdminController\AccessoryController@datatable')->name('product_accessory_dataload');
                    Route::post('/productcategories', 'AdminController\ProductModelController@productCategories')->name('product_categories_load');

                });

                Route::group(['prefix' => 'mobile-condition'], function () {

                    Route::get('/', 'AdminController\MobileConditionController@index')->name('mobile_condition_index');
                    Route::get('/show', 'AdminController\MobileConditionController@show')->name('mobile_condition_show');
                    Route::get('/create', 'AdminController\MobileConditionController@create')->name('mobile_condition_create');
                    Route::post('/store', 'AdminController\MobileConditionController@store')->name('mobile_condition_store');
                    Route::get('/edit/{condition_id}', 'AdminController\MobileConditionController@edit')->name('mobile_condition_edit');
                    Route::post('/dataload', 'AdminController\MobileConditionController@datatable')->name('mobile_condition_dataload');
                    Route::post('/subcategories', 'AdminController\MobileConditionController@subcategories')->name('mobile_condition_subcategories_load');

                });
                Route::group(['prefix' => 'mobile-storage'], function () {

                    Route::get('/', 'AdminController\MobileStorageController@index')->name('mobile_storage_index');
                    Route::get('/show', 'AdminController\MobileStorageController@show')->name('mobile_storage_show');
                    Route::get('/create', 'AdminController\MobileStorageController@create')->name('mobile_storage_create');
                    Route::post('/store', 'AdminController\MobileStorageController@store')->name('mobile_storage_store');
                    Route::get('/edit/{storage_id}', 'AdminController\MobileStorageController@edit')->name('mobile_storage_edit');
                    Route::post('/dataload', 'AdminController\MobileStorageController@datatable')->name('mobile_storage_dataload');
                    Route::post('/subcategories', 'AdminController\MobileStorageController@subcategories')->name('mobile_storage_subcategories_load');

                });
                Route::group(['prefix' => 'mobile-color'], function () {

                    Route::get('/', 'AdminController\MobileColorController@index')->name('mobile_color_index');
                    Route::get('/show', 'AdminController\MobileColorController@show')->name('mobile_color_show');
                    Route::get('/create', 'AdminController\MobileColorController@create')->name('mobile_color_create');
                    Route::post('/store', 'AdminController\MobileColorController@store')->name('mobile_color_store');
                    Route::get('/edit/{color_id}', 'AdminController\MobileColorController@edit')->name('mobile_color_edit');
                    Route::post('/dataload', 'AdminController\MobileColorController@datatable')->name('mobile_color_dataload');
                    Route::post('/subcategories', 'AdminController\MobileColorController@subcategories')->name('mobile_color_subcategories_load');

                });

                Route::namespace('AdminController')->group(function (){
                    Route::group(['prefix' => 'categories'], function () {

                        Route::get('/', 'CategoryController@index')->name('category_index');
                        Route::get('/show', 'CategoryController@show')->name('category_show');
                        Route::get('/create', 'CategoryController@create')->name('category_create');
                        Route::post('/store', 'CategoryController@store')->name('category_store');
                        Route::get('/edit/{category_id}', 'CategoryController@edit')->name('category_edit');
                        Route::post('/dataload', 'CategoryController@datatable')->name('category_dataload');
                        Route::post('/subcategories', 'CategoryController@subcategories')->name('categories_subcategories_load');

                    });

                    Route::group(['prefix' => 'subcategories'], function () {

                        Route::get('/', 'SubcategoryController@index')->name('subcategory_index');
                        Route::get('/show', 'SubcategoryController@show')->name('subcategory_show');
                        Route::get('/create', 'SubcategoryController@create')->name('subcategory_create');
                        Route::post('/store', 'SubcategoryController@store')->name('subcategory_store');
                        Route::get('/edit/{subcategory_id}', 'SubcategoryController@edit')->name('subcategory_edit');
                        Route::post('/dataload', 'SubcategoryController@datatable')->name('subcategory_dataload');
                    });

                    Route::group(['prefix' => 'photos'], function () {

                        Route::get('/', 'PhotoController@index')->name('photo_index');
                        Route::get('/show', 'PhotoController@show')->name('photo_show');
                        Route::get('/create', 'PhotoController@create')->name('photo_create');
                        Route::post('/store', 'PhotoController@store')->name('photo_store');
                        Route::get('/edit/{photo_id}', 'PhotoController@edit')->name('photo_edit');
                        Route::post('/dataload', 'PhotoController@datatable')->name('photo_dataload');
                    });

                    Route::group(['prefix' => 'blog-categories'], function () {

                        Route::get('/', 'BlogCategoryController@index')->name('blog_category_index');
                        Route::get('/show', 'BlogCategoryController@show')->name('blog_category_show');
                        Route::get('/create', 'BlogCategoryController@create')->name('blog_category_create');
                        Route::post('/store', 'BlogCategoryController@store')->name('blog_category_store');
                        Route::get('/edit/{category_id}', 'BlogCategoryController@edit')->name('blog_category_edit');
                        Route::post('/dataload', 'BlogCategoryController@datatable')->name('blog_category_dataload');
                        Route::post('/subcategories', 'BlogCategoryController@subcategories')->name('blog_categories_subcategories_load');

                    });

                    Route::group(['prefix' => 'blog-subcategories'], function () {

                        Route::get('/', 'BlogSubcategoryController@index')->name('blog_subcategory_index');
                        Route::get('/show', 'BlogSubcategoryController@show')->name('blog_subcategory_show');
                        Route::get('/create', 'BlogSubcategoryController@create')->name('blog_subcategory_create');
                        Route::post('/store', 'BlogSubcategoryController@store')->name('blog_subcategory_store');
                        Route::get('/edit/{subcategory_id}', 'BlogSubcategoryController@edit')->name('blog_subcategory_edit');
                        Route::post('/dataload', 'BlogSubcategoryController@datatable')->name('blog_subcategory_dataload');

                    });

                    Route::group(['prefix' => 'blogs'], function () {

                        Route::get('/', 'BlogController@index')->name('blog_index');
                        Route::get('/show', 'BlogController@show')->name('blog_show');
                        Route::get('/create', 'BlogController@create')->name('blog_create');
                        Route::post('/store', 'BlogController@store')->name('blog_store');
                        Route::get('/edit/{blog_id}', 'BlogController@edit')->name('blog_edit');
                        Route::post('/dataload', 'BlogController@datatable')->name('blog_dataload');
                    });



                });

            });




            Auth::routes();
            Route::post('admin/logout', 'Auth\AdminLoginController@AdminLogout')->name('admin_logout');
            //Route::get('/admin/login', 'Auth\AdminLoginController@login')->name('admin_login');
